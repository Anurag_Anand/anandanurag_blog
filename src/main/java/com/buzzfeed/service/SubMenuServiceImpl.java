/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.service;

import com.buzzfeed.models.SubMenu;
import com.buzzfeed.repository.SubMenuRepository;
import java.util.Collection;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Anurag Anand
 */
@Service
public class SubMenuServiceImpl implements SubMenuService{

    @Autowired
    SubMenuRepository submenuRepository;
    
    @Override
    public void save(Map<String,String> subMenuValues , String menuid) throws DataAccessException {
        if(null == subMenuValues || null == menuid 
                 || menuid.trim().length() < 1){
            return;
        }
        
        SubMenu subMenu =new SubMenu();
        subMenu.setName(subMenuValues.get("submenuName"));
        subMenu.setSubMenuURL(subMenuValues.get("subMenuURL"));
        subMenu.setSubMenuTitle(subMenuValues.get("subMenuTitle"));
        subMenu.setSubMenuDescription(subMenuValues.get("subMenuDescription"));
        subMenu.setSubMenuKeywords(subMenuValues.get("subMenuKeywords"));
       submenuRepository.saveMenu(subMenu,menuid);
    }

    @Override
    public void delete(String menuid,String subMenuId) throws DataAccessException {
        if(menuid == null || menuid.trim().length() <1
                || subMenuId == null || subMenuId.trim().length() <1){
            return;
        }
        submenuRepository.deleteMenu(menuid,subMenuId);
    }

    @Override
    public void update(Map<String,String> subMenuValues,String id) throws DataAccessException {
        if(subMenuValues == null||  id == null || id.trim().length() <1){
            return;
        }
        
        submenuRepository.updateMenu(subMenuValues, id);
    }

    @Override
    public SubMenu getById(String id) throws DataAccessException {
        if(id == null || id.trim().length() <1){
            return null;
        }     
        return submenuRepository.getById(id);
    }

    @Override
    public Collection<SubMenu> getAll() {
       return submenuRepository.getAllSubMenu();
    }

    @Override
    public SubMenu getByUrl(String url) throws DataAccessException {
        SubMenu subMenu = submenuRepository.getBySubMenuByURL(url);
        return subMenu;                
    }
    
}
