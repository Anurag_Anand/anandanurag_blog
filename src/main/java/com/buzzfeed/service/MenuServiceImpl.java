/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.service;

import com.buzzfeed.models.Menu;
import com.buzzfeed.repository.MenuRepository;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Anurag Anand
 */
@Service
public class MenuServiceImpl implements MenuService{

    @Autowired
    MenuRepository menuRepository;
    
    @Override
    public void save(Menu menu) throws DataAccessException {
       menuRepository.saveMenu(menu);
    }

    @Override
    public void delete(String id) throws DataAccessException {
        menuRepository.deleteMenu(id);
    }

    @Override
    public void update(String name, String id) throws DataAccessException {
        menuRepository.updateMenu(name, id);
    }

    @Override
    public Collection<Menu> getAllMenu() throws DataAccessException {
        return menuRepository.getAllMenu();
    }

    @Override
    public Menu getById(String id) throws DataAccessException {
        return menuRepository.getById(id);
    }
    
    @Override
    public Collection<Menu> getAllMenuSubMenu() throws DataAccessException{
        return  menuRepository.getAllMenuSubMenu();
    }
    
}
