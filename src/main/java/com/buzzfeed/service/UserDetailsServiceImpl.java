/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.service;

import com.buzzfeed.models.UserDetails;
import com.buzzfeed.models.UserDetailsJson;
import com.buzzfeed.repository.UserDetailsRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Anurag Anand
 */
public class UserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    UserDetailsRepository userDetailsRepository;
    
    @Override
    public void saveUserDetails(String userName,String userPassword,String userRole) throws DataAccessException {
        userDetailsRepository.saveUserDetails(userName, userPassword, userRole);
    }

    @Override
    public void updateUserDetails(String userName,String userPassword) throws DataAccessException {
        userDetailsRepository.updateUserDetails(userName, userPassword);
    }

    @Override
    public void init() throws DataAccessException {
        userDetailsRepository.init();
    }

    @Override
    public void deleteUserDetails(String userName) throws DataAccessException {
        userDetailsRepository.deleteUserDetails(userName);
                }
    
    @Override
    public List<UserDetails> getAllUsers() throws DataAccessException{
        return userDetailsRepository.getAllUsers();
    }
    
    @Override 
    public List<UserDetailsJson> getUserJson(){
        return userDetailsRepository.getUserJson();
    }
}
