/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.service;

import com.buzzfeed.models.SubMenu;
import java.util.Collection;
import java.util.Map;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Anurag Anand
 */
public interface SubMenuService {
    //Persists a Menu in the database
    void save(Map<String,String> subMenuValues,String menuid) throws DataAccessException;
    
    //Delete Menu from table
    void delete(String id,String subMenuId) throws DataAccessException;
    
    //Update Menu name
    void update(Map<String,String> subMenuValues,String id) throws DataAccessException;
    
    //Get SubMenu By Id
    SubMenu getById(String id) throws DataAccessException;
    
    //Get all submenu
    Collection<SubMenu> getAll() throws DataAccessException;
    
    //Get SubMenu By URL
    SubMenu getByUrl(String url) throws DataAccessException;
}
