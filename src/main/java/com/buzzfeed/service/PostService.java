/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.service;

import com.buzzfeed.models.TutorialPost;
import java.util.Collection;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author Anurag Anand
 */
@Component
public interface PostService {
    Long savePost(TutorialPost post) throws DataAccessException;
    Long updatePost(TutorialPost post) throws DataAccessException;
    void deletePost(String uniqueUrlIdentification) throws DataAccessException;    
    TutorialPost getPostByUrlIndentificationTag(String urlIdentificationTag) throws DataAccessException;
    Collection<TutorialPost> listPost(int startRowNumber) throws DataAccessException;

    //Get all post in Database
    Collection<TutorialPost> getAllPostBySubMenu(String subMenuName) throws DataAccessException;
   // void savePost(TutorialPost post) throws DataAccessException;
    
}
