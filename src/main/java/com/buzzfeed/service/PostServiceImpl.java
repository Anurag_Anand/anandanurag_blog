/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.service;

import com.buzzfeed.models.TutorialPost;
import com.buzzfeed.repository.PostRepository;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Anurag Anand
 */
@Service
public class PostServiceImpl implements PostService{
    
    @Autowired
    private PostRepository postRepository; 

    @Override
    @Transactional
    public Long savePost(TutorialPost post) throws DataAccessException {
        return postRepository.savePost(post);
    }

    @Override
    public Long updatePost(TutorialPost post) throws DataAccessException {
          return  postRepository.updatePost(post);
    }
    
    @Override
    @Transactional
    public TutorialPost getPostByUrlIndentificationTag(String urlIdentificationTag) throws DataAccessException {
        return postRepository.findByUrlIdentificationTag(urlIdentificationTag);
    }

    @Override
    @Transactional
    public Collection<TutorialPost> listPost(int startRowNumber) throws DataAccessException {
        return postRepository.getAllPost(startRowNumber);
    }

    @Override
    @Transactional
    public void deletePost(String uniqueUrlIdentification) throws DataAccessException {
        postRepository.deletePost(uniqueUrlIdentification);
    }

    @Override
    public Collection<TutorialPost> getAllPostBySubMenu(String subMenuName) throws DataAccessException {
        return postRepository.getAllPostBySubMenu(subMenuName);
    }

   
}
