/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


/**
 *
 * @author Anurag Anand
 */
@Configuration
@EnableWebSecurity
@Order(3)
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    
    @Autowired
    DriverManagerDataSource dataSource;
  /*  
    @Bean
    public DelegatingFilterProxy springSecurityFilterChain(){
        return new DelegatingFilterProxy();
    }
    */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        //auth.
        //auth.
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(queryUser())
                .authoritiesByUsernameQuery(queryRole());
        
//        auth.inMemoryAuthentication()
//                .withUser("anurag")
//                .password("anurag")
//                .roles("USER");
        
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        
//        http.authorizeRequests()
//                .antMatchers("/admin/**")
//                .access("hasRole('ROLE_USER')")
        http.authorizeRequests()
                .antMatchers("/admin/**")
                .fullyAuthenticated()
                .and()
                .formLogin().loginPage("/login").permitAll().failureUrl("/login")
                .defaultSuccessUrl("/admin")
                .usernameParameter("username").passwordParameter("password")
                .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/logout")
                .and()
                .exceptionHandling().accessDeniedPage("/403")
                .and()
                .csrf();
        // .logoutSuccessUrl("/logout")
    }
    
    private String queryUser(){
        return "select username,password,enabled from UserDetails where username = ?";
    }
    
    private String queryRole(){
        return "select distinct ud.username,ur.rolename from UserDetails ud ,UserRole ur ,UserRoleMap urm "
                + " where ud.id = urm.User_ID and ur.id = urm.Role_ID and "
                + "ud.username = ?";
//        return "select distinct ud.username,ur.rolename from UserDetails ud ,UserRole ur ,UserRoleMap urm "
//                + " where ud.id = urm.User_ID and ur.id = urm.Role_ID and "
//                + "ud.UserName = ?";
    }
}
