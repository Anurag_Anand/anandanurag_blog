/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author Anurag Anand
 */
@Configuration
@Order(2)
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer{
//    public SpringSecurityInitializer() {
//        super(SecurityConfig.class);
//    }
}
