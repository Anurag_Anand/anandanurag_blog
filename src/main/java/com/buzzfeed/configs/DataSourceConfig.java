/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.configs;


import com.buzzfeed.models.CommentsPreview;
import com.buzzfeed.models.Menu;
import com.buzzfeed.models.SubMenu;
import com.buzzfeed.models.TutorialPost;
import com.buzzfeed.models.UserDetails;
import com.buzzfeed.models.UserRole;
import com.buzzfeed.repository.PostRepositoryImpl;
import com.buzzfeed.repository.UserDetailsRepository;
import com.buzzfeed.repository.UserDetailsRepositoryImpl;
import com.buzzfeed.service.UserDetailsService;
import com.buzzfeed.service.UserDetailsServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Anurag Anand
 */
@Configuration
@EnableTransactionManagement
@PropertySource("classpath:/datasource.properties")
@Order(1)
public class DataSourceConfig  {
    
    @Autowired
    Environment env;
    
    protected final Log logger = LogFactory.getLog(getClass());
    static{
        System.out.println("Datasource config claas loaded");
    }
    @Bean
    public DriverManagerDataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("Driver.ClassName"));
        dataSource.setUsername(env.getProperty("Database.UserName"));
        dataSource.setPassword(env.getProperty("Database.Password"));
        dataSource.setUrl(env.getProperty("Database.Url"));
        logger.info("DataSource Initialized");
        return dataSource;
    }
    
    @Bean
    @Autowired
    public LocalSessionFactoryBean sessionFactory(DriverManagerDataSource dataSource){
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setAnnotatedClasses(
                CommentsPreview.class
                ,TutorialPost.class
                ,Menu.class
                ,SubMenu.class
                ,UserDetails.class
                ,UserRole.class);
        Resource configLocation = new ClassPathResource("hibernate.cfg.xml");
        sessionFactory.setConfigLocation(configLocation);
        logger.info("sessionFactory Initialized");
        return sessionFactory;
    }
    
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory){
       
        HibernateTransactionManager hibernateTransactionManager = 
                new HibernateTransactionManager(sessionFactory);
        logger.info("hibernateTransactionManager Initialized");
        return hibernateTransactionManager;
    }
    
     @Bean
    PostRepositoryImpl postRepository(){
        return new PostRepositoryImpl();
    }
    
    @Bean
    UserDetailsService getUserDetailsService(){
        return new UserDetailsServiceImpl();
    }
    
    @Bean
    UserDetailsRepository getUserDetailsRepository(){
        return new UserDetailsRepositoryImpl();
    }
}
