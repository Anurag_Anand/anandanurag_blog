/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.Menu;
import com.buzzfeed.models.SubMenu;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Anurag Anand
 */
public interface MenuRepository {
    //Persists a Menu in the database
    void saveMenu(Menu menu) throws DataAccessException;
    
    //Delete Menu from table
    void deleteMenu(String id) throws DataAccessException;
    
    //Update Menu name
    void updateMenu(String name,String id) throws DataAccessException;
    
    //Return List of all menu
    Collection<Menu> getAllMenu() throws DataAccessException; 
    
    //Get a Menu By it`s ID
    Menu getById(String id) throws DataAccessException;
    
    //Get all Menu and submenu
    Collection<Menu> getAllMenuSubMenu() throws DataAccessException ;
    //HashMap<String,List<SubMenu>> getAllMenuSubMenu() throws DataAccessException ;
}
