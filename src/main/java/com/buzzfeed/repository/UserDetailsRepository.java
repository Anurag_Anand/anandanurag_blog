/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.UserDetails;
import com.buzzfeed.models.UserDetailsJson;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Anurag Anand
 */
public interface UserDetailsRepository {
    
    //Save User Data
    void saveUserDetails(String userName,String userPassword,String userRole) throws DataAccessException;
    
    //Update Password
    void updateUserDetails(String userName,String userPassword) throws DataAccessException;
    
    //Called to insert First row of user details so that user can login..
    void init() throws DataAccessException;
    
    //Deletes User from database
    void deleteUserDetails(String userName) throws DataAccessException;
    
    //Get All fucking users
    List<UserDetails> getAllUsers() throws DataAccessException;
    
    //Get  All fucking users as JSON
    List<UserDetailsJson> getUserJson() throws DataAccessException;
    
}
