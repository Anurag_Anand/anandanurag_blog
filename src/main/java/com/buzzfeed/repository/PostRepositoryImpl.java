/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.SubMenu;
import com.buzzfeed.models.TutorialPost;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Anurag Anand
 */
@Repository
public class PostRepositoryImpl implements PostRepository{
    
    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long savePost(TutorialPost post) throws DataAccessException {
        Session session = sessionFactory.openSession();
        Transaction txn = session.beginTransaction();
        post.setDateOfCreation(new Date());
        post.setDateOfModification(new Date());        
        session.saveOrUpdate(post);        
        txn.commit();
        session.close();
        Long postId = post.getId();
        return postId;
    }

    @Override
    public Long updatePost(TutorialPost post) throws DataAccessException {
        String hql = "update TutorialPost set "
                + "title = :title"
                + ",dateOfModification = :dateOfModification"
                + ",postContent = :postContent"
                + ",publishPost = :publishPost"
                + ",brief = :brief"
                + ",subMenuId = :subMenuId"
                + ",keyword = :keyword"
                + " where postUrlIdentificationTag = :postUrlIdentificationTag";
        Session session = sessionFactory.openSession();
        Transaction txn = session.beginTransaction();
        //Updating post
            Query query  = session.createQuery(hql);
            query.setString("title", post.getTitle());
            query.setDate("dateOfModification", new Date());
            query.setString("postContent", post.getPostContent());
            query.setBoolean("publishPost", post.isPublishPost());
            query.setString("brief", post.getBrief());
            query.setString("subMenuId", post.getsubMenuId());
            query.setString("keyword", post.getKeyword());
            query.setString("postUrlIdentificationTag", post.getPostUrlIdentificationTag());
            query.executeUpdate();
            System.out.println("Update Query is "+query.toString());
            
        txn.commit();
        session.close();
        Long postId = post.getId();
        return postId;
    }

    
    @Override
    public TutorialPost findByUrlIdentificationTag(String urlTag) throws DataAccessException {
         Session session = sessionFactory.openSession();
        List<TutorialPost> postList = session.createQuery("from TutorialPost post "
                + "where post.postUrlIdentificationTag"
                + "= :UrlIdentification")
                .setString("UrlIdentification", urlTag)
                .setFetchSize(1)
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.TutorialPost")
                .list();
        
        
        TutorialPost post = postList.size() > 0 ? postList.get(0) : null;
        session.close();
        return post;
    }

    @Override
    public List<TutorialPost> getAllPost(int startRowNumber) throws DataAccessException {
        Session session = sessionFactory.openSession();
        List<TutorialPost> postList = session.createQuery("from TutorialPost tutPost "
                + " where tutPost.publishPost = :publishPost")
                .setBoolean("publishPost", true)
                .setFetchSize(15)
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.TutorialPost")                
                .list();
        session.close();
        return postList;
    }

    @Override
    public List<TutorialPost> getPostByTag(String tag) throws DataAccessException {
        throw new UnsupportedOperationException("Searching a Post on tag is not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
//    public Collection<String> getTags() throws DataAccessException {
//        Session session = sessionFactory.openSession();
//        List<String> tagList = session.createSQLQuery("Select distinct tags from posttags")
//                .list();
//        session.close();
//        return tagList;
//    }

    @Override
    public void deletePost(String uniqueUrlId) throws DataAccessException {
     Session session = sessionFactory.openSession();
     Transaction txn = session.beginTransaction();     
     List<TutorialPost> postList = session.createQuery("from TutorialPost post "
                + "where post.postUrlIdentificationTag"
                + "= :UrlIdentification")
                .setString("UrlIdentification", uniqueUrlId)
                .setFetchSize(1)
                .list();
        
        
        TutorialPost post = postList.size() > 0 ? postList.get(0) : null;
     if(post != null){
         session.delete(post);
     }          
     txn.commit();
     session.close();     
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<TutorialPost> getAllPostBySubMenu(String subMenuURL) throws DataAccessException {
        
        Session session = sessionFactory.openSession();
                
                List<SubMenu> subMlist = session.createQuery("from SubMenu m where m.subMenuURL = :subMenuURL")
                .setString("subMenuURL", subMenuURL)
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.SubMenu")
                .list();
                if(subMlist == null || subMlist.size() < 1){
                    throw new IllegalArgumentException("The following subMenuURL "+subMenuURL
                            + " is not present in table ");
                }
            SubMenu subM = subMlist.get(0);
            List<TutorialPost> postList = session.createQuery("from TutorialPost post"
                    + " where post.subMenuId = :subMenuId and post.publishPost = :publishPost")
                    .setString("subMenuId", subM.getId().toString())
                    .setBoolean("publishPost", true)
                .setFetchSize(15)
                    .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.TutorialPost")
                .list();    
        session.close();
        return postList;
     
    }
//
//    @Override
//    public Collection<String> getTagsPerPost(TutorialPost tutorialPost) throws DataAccessException {
//        Session session = sessionFactory.openSession();
//        List<String> tagList = session.createSQLQuery("Select distinct tags from posttags where "
//                + " Post_Id = :Post_Id")
//                .setLong("Post_Id", tutorialPost.getId())  
//                .list();
//        session.close();
//        return tagList;
//    }
//    
//    
//    
}
