/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.Menu;
import java.util.Collection;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Anurag Anand
 */
@Repository
public class MenuRepositoryImpl implements MenuRepository{

    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMenu(Menu menu) throws DataAccessException {
        if(menu != null){
            Session session = sessionFactory.openSession();
            Transaction txn = session.beginTransaction();
            
            session.save(menu);
            
            txn.commit();
            session.close();
            
        }
    }

    @Override
    public void deleteMenu(String id) throws DataAccessException {
        if(id == null || id.trim().length() <1){
            return;
        }
        
            Session session = sessionFactory.openSession();
            Transaction txn = session.beginTransaction();
            Menu menu = (Menu) session.get(Menu.class, new Long(id.trim()));
            session.delete(menu);
            
            txn.commit();
            session.close();
            
    }

    @Override
    public void updateMenu(String name, String id) throws DataAccessException {
        if(name == null || name.trim().length() < 1 ||  id == null || id.trim().length() <1){
            return;
        }
        
            Session session = sessionFactory.openSession();
            Transaction txn = session.beginTransaction();
            Menu menu = (Menu) session.get(Menu.class, new Long(id.trim()));
            menu.setName(name);
            session.update(menu);
            txn.commit();
            session.close();
    
    }

    @Override
    public Collection<Menu> getAllMenu() throws DataAccessException {
        Collection<Menu> menuList ;
        Session session = sessionFactory.openSession();
        menuList = session.createQuery("from Menu")
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.Menu")
                .list();
        session.close();
        return menuList;
    }

    @Override
    public Menu getById(String id) throws DataAccessException {
        Session session = sessionFactory.openSession();
        Menu menu = null;
        List<Menu> menuList = session.createQuery("from Menu menu where menu.id = :menuid ")
                .setLong("menuid", new Long(id))
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.Menu")
                .list();
        if(menuList.size() > 0 ){
            menu = menuList.get(0);
        }
        session.close();
        
        return menu;
    }
    
    
    @Override
    public Collection<Menu> getAllMenuSubMenu() throws DataAccessException{
   //    HashMap<String,List<SubMenu>>  hmap = new HashMap<>();
       Collection<Menu> menuList = null;
//        System.out.println("Inside getAllMenuSubMenu ...");
       
//       Session session = sessionFactory.openSession();
//       menuList = session.createQuery("from Menu menu join fetch menu.subMenu submenu where menu.id = submenu.menu.id")
//                 .setCacheable(true)
//                 .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
//                 .list();       
        menuList = getAllMenu();
//        session.close(); 
//        System.out.println("Leaving getAllMenuSubMenu ...");
        return menuList;        
    }
    
}
