/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.UserDetails;
import com.buzzfeed.models.UserDetailsJson;
import com.buzzfeed.models.UserRole;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Anurag Anand
 */
@Repository
public class UserDetailsRepositoryImpl implements UserDetailsRepository{

    @Autowired
    SessionFactory sessionFactory;
    
    @Override   
    @Transactional(rollbackFor = Exception.class)
    public void saveUserDetails(String userName,String userPassword,String userRoleName) throws DataAccessException {
    Session session = sessionFactory.openSession();    
        Transaction txn = session.beginTransaction();
        //Create a new object for user 
        UserDetails userNew = new UserDetails();        
        //Create new userrole
        UserRole userRole =  new UserRole();        
            //Set User Role
            userRole.setRoleName(userRoleName);
            //Add newly created User
            userRole.getUserDetails().add(userNew);
            
            //set information for newly created user
            userNew.setUserName(userName);//User Name set :)
            userNew.setPassword(userPassword);//user Password Set :)
            userNew.getUserRoles().add(userRole);//user role set :)
            userNew.setEnabled(true);//Enable new User
            session.save(userNew);//save user to database :)
        txn.commit();    
    session.close();
    }

    @Override
    public void updateUserDetails(String userName,String userPassword) throws DataAccessException {
        Session session = sessionFactory.openSession();
        Transaction txn = session.beginTransaction();
        //get user object
            List<UserDetails> userList = session.createQuery("from UserDetails user where user.userName = :userName")
                                        .setString("userName",userName)
                                        .list();
            
            UserDetails user = null;
            
            //If exists
            if(userList.size() > 0 ){
                //then set it to user object
                user = userList.get(0);
                //update with the new password
                user.setPassword(userPassword);
                //update it to the database
                session.update(user);
            }
            
        txn.commit();
        session.close();
    }

    //It is used to add the first username and password to the database for first time login
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void init() throws DataAccessException {
        Session session = sessionFactory.openSession();
        List<UserDetails> users = session.createQuery("from UserDetails")
                                         .list();
        System.out.println("total number of users are "+users.size());
        if(users.size() < 1 ){
            System.out.println("No data found in table..adding new row");
            UserDetails userInit = new UserDetails();
            UserRole userRole =  new UserRole();
            
            userRole.setRoleName("USER");
            userRole.getUserDetails().add(userInit);
            
            userInit.setEnabled(true);
            userInit.setPassword("anurag789");
            userInit.setUserName("789anurag789");
            userInit.getUserRoles().add(userRole);
            
            Transaction txn = session.beginTransaction();            
                 session.save(userInit);
            txn.commit();
        }
        session.close();
    }

    @Override
    public void deleteUserDetails(String userName) throws DataAccessException {
        Session session = sessionFactory.openSession();
        Transaction txn = session.beginTransaction();
        //get user object
            List<UserDetails> userList = session.createQuery("from UserDetails user where user.userName = :userName")
                                        .setString("userName",userName)
                                        .list();
            
            UserDetails user = null;
            
            //If exists
            if(userList.size() > 0 ){
                //then set it to user object
                user = userList.get(0);                
                //delete it from the database
                session.delete(user);
            }
            
        txn.commit();
        session.close();
    }
    
    @Override
    public List<UserDetails> getAllUsers() throws DataAccessException{
        Session session = sessionFactory.openSession();
        List<UserDetails> users = session.createQuery("from UserDetails")
                                         .list();
        session.close();
        return users;
    }
    
    @Override
    public List<UserDetailsJson> getUserJson(){
        Session session = sessionFactory.openSession();
        List<UserDetails> users = session.createQuery("from UserDetails")
                                         .list();
        List<UserDetailsJson> userJson = new ArrayList<>();
        
        for(UserDetails u : users){
            UserDetailsJson detailsJson = new UserDetailsJson();
            detailsJson.setUserName(u.getUserName());
            detailsJson.setUserEnabled(u.isEnabled());
            detailsJson.setUserRole(u.getUserRoles().get(0).getRoleName());
            userJson.add(detailsJson);
        }
        
        session.close();
        return userJson;
    }
    
}
