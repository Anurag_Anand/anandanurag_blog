/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.TutorialPost;
import java.util.Collection;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Anurag Anand
 */
public interface PostRepository {
    
    //Save post to database
    Long savePost(TutorialPost post) throws DataAccessException;
    
    //Update post to database
    Long updatePost(TutorialPost post) throws DataAccessException;
    
    //delete post from database
    void deletePost(String Id) throws DataAccessException;
    
    //Get a paricular Post from Database by PostUrlIdentificationTag
    TutorialPost findByUrlIdentificationTag(String urlTag) throws DataAccessException;
    
    //Get all post in Database
    Collection<TutorialPost> getAllPostBySubMenu(String subMenuName) throws DataAccessException;
    
    //Get all post in Database
    Collection<TutorialPost> getAllPost(int startRowNumber) throws DataAccessException;
    
    //Get post by tag
    Collection<TutorialPost> getPostByTag(String tag) throws DataAccessException;
    
 }
