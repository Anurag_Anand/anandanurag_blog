/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.SubMenu;
import java.util.Collection;
import java.util.Map;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Anurag Anand
 */
public interface SubMenuRepository {
    //Persists a Menu in the database
    void saveMenu(SubMenu submenu,String menuid) throws DataAccessException;
    
    //Delete Menu from table
    void deleteMenu(String menuid,String subMenuId) throws DataAccessException;
    
    //Update Menu name
    void updateMenu(Map<String,String> subMenuValues,String id) throws DataAccessException;
    
    //Get SubMenu By sub menu ID
    SubMenu getById(String id) throws DataAccessException;
    
    //
    Collection<SubMenu> getAllSubMenu() throws DataAccessException;
    
    //Get SubMenu by SubMneu URL
    SubMenu getBySubMenuByURL(String subMenuURL) throws DataAccessException;
}
