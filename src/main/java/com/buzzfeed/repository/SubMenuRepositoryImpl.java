/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.repository;

import com.buzzfeed.models.Menu;
import com.buzzfeed.models.SubMenu;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Anurag Anand
 */
@Repository
public class SubMenuRepositoryImpl implements SubMenuRepository{

    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMenu(SubMenu submenu,String menuid) throws DataAccessException {
        
            Session session = sessionFactory.openSession();
            Transaction txn = session.beginTransaction();
            Menu menu = (Menu) session.get(Menu.class, new Long(menuid));
            submenu.setMenu(menu);
            menu.getSubMenu().add(submenu);
            session.update(menu);
            txn.commit();
            session.close();
    }

    @Override
    public void deleteMenu(String menuid,String subMenuId) throws DataAccessException {
        
            Session session = sessionFactory.openSession();
            Transaction txn = session.beginTransaction();
            Menu menu = (Menu) session.get(Menu.class, new Long(menuid.trim()));
            SubMenu submenu = (SubMenu) session.get(SubMenu.class, new Long(subMenuId.trim()));
            menu.getSubMenu().remove(submenu);
            session.update(menu);
            txn.commit();
            session.close();
    }

    @Override
    public void updateMenu(Map<String,String> subMenuValues, String id) throws DataAccessException {        
            Session session = sessionFactory.openSession();
            Transaction txn = session.beginTransaction();
            SubMenu subMenu = (SubMenu) session.get(SubMenu.class, new Long(id.trim()));
            subMenu.setName(subMenuValues.get("submenuName"));
        subMenu.setSubMenuURL(subMenuValues.get("subMenuURL"));
        subMenu.setSubMenuTitle(subMenuValues.get("subMenuTitle"));
        subMenu.setSubMenuDescription(subMenuValues.get("subMenuDescription"));
        subMenu.setSubMenuKeywords(subMenuValues.get("subMenuKeywords"));
            session.update(subMenu);
            txn.commit();
            session.close();
    
    }

    @Override
    public SubMenu getById(String id) throws DataAccessException {
            Session session = sessionFactory.openSession();
            SubMenu submenu = null;
            List<SubMenu> subMenuList = session.createQuery("from SubMenu submenu "
                    + " where submenu.id = :submenuid")
                    .setLong("submenuid", new Long(id))
                    .setCacheable(true)
                    .setCacheRegion("com.buzzfeed.models.SubMenu")
                    .list();
            
            if(! subMenuList.isEmpty()){
                submenu = subMenuList.get(0);
            }
            
            session.close();            
            return submenu;
    }

    @Override
    public Collection<SubMenu> getAllSubMenu() throws DataAccessException {
        Session session = sessionFactory.openSession();
        Collection<SubMenu> submenu = session.createQuery("from SubMenu")
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.SubMenu")
                .list();        
        session.close();
        return submenu;
    }

    @Override
    public SubMenu getBySubMenuByURL(String subMenuURL) throws DataAccessException {
        Session session = sessionFactory.openSession();
        List<SubMenu> subMenuList = session.createQuery("from SubMenu subMenu where subMenu.subMenuURL"
                + " = :subMenuURL")
                .setString("subMenuURL", subMenuURL)
                .setCacheable(true)
                .setCacheRegion("com.buzzfeed.models.SubMenu")
                .list();
        SubMenu subMenu = null;
        if(subMenuList.size() > 0){
            subMenu = subMenuList.get(0);
        }
        
        session.close();
        return subMenu;
    }
    
    
}
