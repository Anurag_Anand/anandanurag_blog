/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.controllers;

import com.buzzfeed.models.Menu;
import com.buzzfeed.models.TutorialPost;
import com.buzzfeed.service.MenuService;
import com.buzzfeed.service.PostService;
import com.buzzfeed.service.SubMenuService;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Anurag Anand
 */
@Controller
//@RequestMapping(value="/")
public class TutorialController {
    protected final Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    private PostService postService;
    
    @Autowired
    private MenuService menuService;
    
    @Autowired
    private SubMenuService subMenuService;
        
    
    //it will load list of tutorials by descending order of creation date, i.e newer to older
    @RequestMapping(value="/",method=RequestMethod.GET)
    public String loadHome(Model model){
        logger.info("Before getting list of all post");
        Collection<TutorialPost> allPost = postService.listPost(1);        
        //Sending list of tutorials
        model.addAttribute("posts",allPost);            
        
      //  HashMap<String,List<SubMenu>>  hmap= menuService.getAllMenuSubMenu();
        Collection<Menu>  menuList= menuService.getAllMenuSubMenu();
        model.addAttribute("menus", menuList);             
        logger.info("Tutorial List Page");
        return "frontPage";
    }
    
    //it will load list of tutorials by descending order of creation date, i.e newer to older
    @RequestMapping(value = {"tutorial/menu","tutorial","page"},method=RequestMethod.GET)
    public String loadFrontPage(Model model){
        logger.info("Before getting list of all post");
        Collection<TutorialPost> allPost = postService.listPost(1);        
        //Sending list of tutorials
        model.addAttribute("posts",allPost);         
        
        //  HashMap<String,List<SubMenu>>  hmap= menuService.getAllMenuSubMenu();
        Collection<Menu>  menuList= menuService.getAllMenuSubMenu();
        model.addAttribute("menus", menuList);
        
        logger.info("Tutorial List Page");
        return "frontPage";
    }
    
    //It loads the page by page number provided in the Pagination
    @RequestMapping(value="page/{pageNumber}",method=RequestMethod.GET)
    public String loadOtherPages(@PathVariable("pageNumber") int pageNumber,Model model){
        logger.info("Before getting list of all post");
        Collection<TutorialPost> allPost = postService.listPost(pageNumber);        
        //Sending list of tutorials
        model.addAttribute("posts",allPost);  
        
        //  HashMap<String,List<SubMenu>>  hmap= menuService.getAllMenuSubMenu();
        Collection<Menu>  menuList= menuService.getAllMenuSubMenu();
        model.addAttribute("menus", menuList); 
        
        logger.info("Tutorial List Page");
        return "frontPage";
    }
    
     @RequestMapping(value="tutorial/menu/{subMenuURL}",method=RequestMethod.GET)
    public String loadBySubMenu(@PathVariable("subMenuURL") String subMenuURL,
            Model model){
        if(null == subMenuURL){
            return "redirect:/";
        }
        logger.info("Before getting list of all post for subMenuURL "+subMenuURL);
        
        Collection<TutorialPost> allposts = postService.getAllPostBySubMenu(subMenuURL);
        model.addAttribute("posts", allposts);               
          
        //  HashMap<String,List<SubMenu>>  hmap= menuService.getAllMenuSubMenu();
        Collection<Menu>  menuList= menuService.getAllMenuSubMenu();
        model.addAttribute("menus", menuList);
        
        logger.info("Tutorial View Tutoirail controller Page");
        return "frontPage";
    }
    
     @RequestMapping(value="tutorial/{uniqueid}",method=RequestMethod.GET)
    public String loadTutorialPages(@PathVariable("uniqueid") String postUniqueUrlIdentification,
            Model model){
        if(null == postUniqueUrlIdentification){
            return "redirect:/";
        }
        logger.info("Before getting list of all post");
        
        TutorialPost tutorialPost = postService.getPostByUrlIndentificationTag(postUniqueUrlIdentification);
        model.addAttribute("post", tutorialPost);               
        
      //  HashMap<String,List<SubMenu>>  hmap= menuService.getAllMenuSubMenu();
        Collection<Menu>  menuList= menuService.getAllMenuSubMenu();
        model.addAttribute("menus", menuList); 
        
        logger.info("Tutorial View Tutoirail controller Page");
        return "viewpost";
    }
    
    @ResponseBody
    @RequestMapping(value="save/comments",method = RequestMethod.GET)
    public String saveComment(HttpServletRequest requestObj,Model model){
        System.out.println("Submit comment is called");
        
        Map<String,String> values = new HashMap<>();
        
        String username = requestObj.getParameter("user_name");
        String comment = requestObj.getParameter("comment_box");
        String emailId = requestObj.getParameter("email_id");
        String postId = requestObj.getParameter("post_id");

        values.put("user_name", username);
        values.put("comment_box", comment);
        values.put("email_id", emailId);
        values.put("post_id", postId);                
        
        return "Saved";
    }
    
    
}
