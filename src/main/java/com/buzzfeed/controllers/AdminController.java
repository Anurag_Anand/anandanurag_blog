/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.controllers;

import com.buzzfeed.models.Menu;
import com.buzzfeed.models.SubMenu;
import com.buzzfeed.models.TutorialPost;
import com.buzzfeed.models.UserDetails;
import com.buzzfeed.models.UserDetailsJson;
import com.buzzfeed.service.MenuService;
import com.buzzfeed.service.PostService;
import com.buzzfeed.service.SubMenuService;
import com.buzzfeed.service.UserDetailsService;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Anurag Anand
 */
@Controller
public class AdminController {
    
    protected final Log logger = LogFactory.getLog(getClass());
    
//    @Autowired
//    @Qualifier("validatePost")
//    private Validator postValidator;
//    
    @Autowired
    private PostService postService;
    
    @Autowired
    private MenuService menuService;
    
    @Autowired
    private SubMenuService subMenuService;
    
    @Autowired
    private UserDetailsService userDetailsService;
        
//    @InitBinder("post")
//    private void initBinder(WebDataBinder dataBinder){
//        dataBinder.registerCustomEditor(Set.class, new TagEditor(Set.class, true));
//        dataBinder.setValidator(postValidator);
//    }
//    
    @ModelAttribute("post")
    public TutorialPost post(){
        return new TutorialPost();
    }
    
    @ModelAttribute("submenu")
    public Map<String,String> submenu(){
        Map<String,String> subMenuMap = new HashMap<>();
        Collection<SubMenu> submenu = subMenuService.getAll();
        for(SubMenu m : submenu){
            subMenuMap.put(m.getId().toString(), m.getName());
        }
        return subMenuMap;
    }
    
    @RequestMapping(value={"/init1/init2/init"} , method = RequestMethod.GET)
    public String initUserDetails(Model model){
        userDetailsService.init();
        return "404";
    }
    
    @RequestMapping(value={"/404"} , method = RequestMethod.GET)
    public String pageNotFound(HttpServletResponse response,Model model){
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return "404";
    }
    
    @RequestMapping(value={"/403"} , method = RequestMethod.GET)
    public String accessDenied(HttpServletResponse response,Model model){
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return "403";
    }
    
    @RequestMapping(value={"/welcome"} , method = RequestMethod.GET)
    public String welcomePage(Model model){
        model.addAttribute("title", "Welcome Page");
        model.addAttribute("message", "You are welcome here");
        System.out.println("Welcome page");
        logger.info("Welcome page");
        return "index";
    }
    
    @RequestMapping(value="/admin",method=RequestMethod.GET)
    public String adminPage(Model model){
        model.addAttribute("title", "Welcome Page");
        model.addAttribute("message", "You are welcome here");
        System.out.println("Admin Page");
        logger.info("Admin page");
        return "admin";
    }
    
    @RequestMapping(value="/admin/new-post",method=RequestMethod.GET)
    public String newPost(Model model){
        logger.info("Tutotrial Create Page");
        return "newpost";
    }
    
    @RequestMapping(value="/admin/save-post",method=RequestMethod.POST)
    public String savePost(@ModelAttribute("post") @Valid TutorialPost post ,
            BindingResult result,Error error,Model model){
        
        if(result.hasErrors()){
     //       return "redirect:/admin/new-post";
        }
        
       Long postId = postService.savePost(post);
            model.addAttribute("post", post);
        logger.info("Tutorial Create Page");
        return "redirect:/admin/preview-post/"+post.getPostUrlIdentificationTag();
    }
    
    @RequestMapping(value="/admin/update-post",method=RequestMethod.POST)
    public String updatePost(@ModelAttribute("post") @Valid TutorialPost post ,
            BindingResult result,Error error,Model model){
        
        if(result.hasErrors()){
     //       return "redirect:/admin/new-post";
        }
        
        
        Long postId = postService.updatePost(post);
        logger.error("Post ID is "+postId);
            model.addAttribute("post", post);
        logger.info("Tutorial Update Page");
        return "redirect:/admin/preview-post/"+post.getPostUrlIdentificationTag();
    }
    
    
    @RequestMapping(value="/admin/list-post",method=RequestMethod.GET)
    public String listPost(Model model){
        logger.info("Before getting list of all post");
        Collection<TutorialPost> allPost = postService.listPost(1);
        //Sending list of tutorials
        model.addAttribute("posts",allPost);
        
        Collection<Menu> menus = menuService.getAllMenu();
        model.addAttribute("menus", menus);                               
            
        logger.info("Tutorial List Page");
        
        return "listAllPost";
    }
    
    @RequestMapping(value="/admin/menu/{subMenuURL}",method=RequestMethod.GET)
    public String loadBySubMenu(@PathVariable("subMenuURL") String subMenuURL,
            Model model){
        if(null == subMenuURL){
            return "redirect:/";
        }
        logger.info("Before getting list of all post for subMenuURL "+subMenuURL);
        
        Collection<TutorialPost> allposts = postService.getAllPostBySubMenu(subMenuURL);
        model.addAttribute("posts", allposts);               
        
        Collection<Menu> menus = menuService.getAllMenu();
        model.addAttribute("menus", menus);               
        
        SubMenu subMenu = subMenuService.getByUrl(subMenuURL);
        model.addAttribute("submenu", subMenu);
        
        logger.info("Admin View Admin controller page");
        return "listAllPost";
    }
    
    @RequestMapping(value="/admin/view-post/{uniqueid}",method=RequestMethod.GET)
    public String viewPost(@PathVariable("uniqueid") String postUniqueUrlIdentification,Model model){
        TutorialPost tutorialPost = postService.getPostByUrlIndentificationTag(postUniqueUrlIdentification);
            model.addAttribute("post", tutorialPost);
        logger.info("Edit Post Page");        
        return "postsuccess";
    }
    
    @RequestMapping(value="/admin/preview-post/{uniqueid}",method=RequestMethod.GET)
    public String previewPost(@PathVariable("uniqueid") String postUniqueUrlIdentification,Model model){
        TutorialPost tutorialPost = postService.getPostByUrlIndentificationTag(postUniqueUrlIdentification);
            model.addAttribute("post", tutorialPost);
        logger.info("Edit Post Page");        
        return "preview";
    }
    
    @RequestMapping(value="/admin/edit-post/{uniqueid}",method=RequestMethod.GET)
    public String editPost(@PathVariable("uniqueid") String postUniqueUrlIdentification,Model model){
            TutorialPost tutorialPost = postService.getPostByUrlIndentificationTag(postUniqueUrlIdentification);
            model.addAttribute("post", tutorialPost);
            
            Collection<Menu> menus = menuService.getAllMenu();
        model.addAttribute("menus", menus); 
        
        logger.info("Edit Post Page");
        return "editPost";
    }
    
//    @ResponseBody
//    @RequestMapping(value="/admin/update/comments/{commentId}",method=RequestMethod.GET)
//    public String updateComment(@PathVariable("commentId") String commentId,
//            Model model){
//        logger.info("comment visibility updated");
//        return "comment updated";
//    }
//    
//    @ResponseBody
//    @RequestMapping(value="/admin/delete/comments/{postid}/{commentid}",method=RequestMethod.GET)
//    public String deletePost(@PathVariable("postid") String postUniqueUrlIdentification,
//            @PathVariable("commentid") String commentId,
//            Model model){
//
//        
//        logger.info("Delete Post");
//        return "commnent Deleted";
//    }
//    
    @RequestMapping(value="/admin/delete-post/{uniqueid}",method=RequestMethod.GET)
    public String deletePost(@PathVariable("uniqueid") String postUniqueUrlIdentification,
            Model model){
        postService.deletePost(postUniqueUrlIdentification);
        logger.info("Delete Post");
        return "redirect:/admin/list-post";
    }
    
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public String loginPage(@RequestParam(value="error",required=false) String error,
         @RequestParam(value="msg",required=false) String msg,Model model){
        
        if(null != msg){
            model.addAttribute("message", msg);
        }
        
        if(null != error){
            model.addAttribute("error", error);
        }
        System.out.println("Login page");
        logger.info("Login page");
        return "login";
    }
    
    @RequestMapping(value="/logout",method=RequestMethod.GET)
    public String logoutPage(Model model){
        model.addAttribute("Message", "You have been successfully logged out");
        System.out.println("logout page");
        logger.info("logout page");
        return "logout";
    }
    
    @RequestMapping(value="/admin/list-menu",method = RequestMethod.GET)
    public String menuList(Model model){
        //move it to menu jsp
        Collection<Menu> menuList = menuService.getAllMenu();
        model.addAttribute("allmenu", menuList);
        return "menu";
    }
    
    @RequestMapping(value="/admin/new-menu",method = RequestMethod.GET)
    public String menu(Model model){
        //move it to menu jsp
        Menu menu = new Menu();
        model.addAttribute("menu", menu);
        return "CreateMenu";
    }
    
    @RequestMapping(value="/admin/save-menu",method = RequestMethod.GET)
    public String createMenu(@ModelAttribute("menu") Menu menu ,Model model){
        menuService.save(menu);
        return "redirect:/admin/list-menu";
    }
    
    @RequestMapping(value="/admin/edit-menu/{menuid}",method = RequestMethod.GET)
    public String editMenu(@PathVariable("menuid") String menuid,Model model){
        Menu menuNew = menuService.getById(menuid);
        model.addAttribute("menu", menuNew);
        return "updatemenu";
    }
    
    @RequestMapping(value="/admin/update-menu/{menuid}",method = RequestMethod.GET)
    public String updateMenu(@PathVariable("menuid") String menuid,HttpServletRequest request){
        
        String name  = request.getParameter("name");
        menuService.update(name, menuid);
        
        return "redirect:/admin/list-menu";
    }
    
    @RequestMapping(value="/admin/delete-menu/{menuid}",method = RequestMethod.GET)
    public String deleteMenu(@PathVariable String menuid,Model model){
        menuService.delete(menuid);
        return "redirect:/admin/list-menu";
    }
    
    @RequestMapping(value="/admin/new-submenu/{menuid}",method = RequestMethod.GET)
    public String submenu(@PathVariable String menuid,Model model){
        //move it to menu jsp
        model.addAttribute("menuid", menuid);
        return "createsubmenu";
    }
    
    @RequestMapping(value="/admin/save-submenu/{menuid}",method = RequestMethod.GET)
    public String createSubMenu(@PathVariable String menuid ,HttpServletRequest request){
        String submenuName = request.getParameter("submenuName");
        String subMenuURL = request.getParameter("subMenuURL");
        String subMenuTitle = request.getParameter("subMenuTitle");
        String subMenuDescription = request.getParameter("subMenuDescription");
        String subMenuKeywords = request.getParameter("subMenuKeywords");
        Map<String,String> subMenuValues = new HashMap<>();
        subMenuValues.put("submenuName", submenuName);
        subMenuValues.put("subMenuURL", subMenuURL);
        subMenuValues.put("subMenuTitle", subMenuTitle);
        subMenuValues.put("subMenuDescription", subMenuDescription);
        subMenuValues.put("subMenuKeywords", subMenuKeywords);
        subMenuService.save(subMenuValues,menuid);
        return "redirect:/admin/list-menu";
    }
    
    @RequestMapping(value="/admin/edit-submenu/{submenuid}",method = RequestMethod.GET)
    public String editSubMenu(@PathVariable("submenuid") String submenuid,Model model){
        SubMenu submenuNew = subMenuService.getById(submenuid);
        model.addAttribute("submenuid", submenuid);
        model.addAttribute("submenu", submenuNew);
        return "editsubmenu";
    }
    
    @RequestMapping(value="/admin/update-submenu/{submenuid}",method = RequestMethod.GET)
    public String updateSubMenu(@PathVariable("submenuid") String submenuid,HttpServletRequest request){
        
        String submenuName = request.getParameter("submenuName");
        String subMenuURL = request.getParameter("subMenuURL");
        String subMenuTitle = request.getParameter("subMenuTitle");
        String subMenuDescription = request.getParameter("subMenuDescription");
        String subMenuKeywords = request.getParameter("subMenuKeywords");
        Map<String,String> subMenuValues = new HashMap<>();
        subMenuValues.put("submenuName", submenuName);
        subMenuValues.put("subMenuURL", subMenuURL);
        subMenuValues.put("subMenuTitle", subMenuTitle);
        subMenuValues.put("subMenuDescription", subMenuDescription);
        subMenuValues.put("subMenuKeywords", subMenuKeywords);
        subMenuService.update(subMenuValues, submenuid);
        return "redirect:/admin/list-menu";
    }
    
    @RequestMapping(value="/admin/delete-submenu/{menuid}/{submenuid}",method = RequestMethod.GET)
    public String deleteSubMenu(@PathVariable String menuid,@PathVariable String submenuid,Model model){
        subMenuService.delete(menuid,submenuid);
        return "redirect:/admin/list-menu";
    }
    
    //manage users
    @RequestMapping(value="/admin/manage-users",method=RequestMethod.GET)
    public String manageUsers(Model model){
        List<UserDetails> userList = userDetailsService.getAllUsers();        
        model.addAttribute("users",userList);
        return "manageuser";
    }
    
    @ResponseBody
    @RequestMapping(value="/admin/getAllUser",method=RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDetailsJson> getAllUsers(){
        List<UserDetailsJson> userList = userDetailsService.getUserJson();        
        return userList;
    }
    
    //update users
    @RequestMapping(value="/admin/updateuser/{userName}/{userPassword}",method = RequestMethod.POST)
    @ResponseBody
    public String updateUsers(@PathVariable String userName,@PathVariable String userPassword){
        String message = userName+" Password Updated Bitch :) !!!";
        
        userDetailsService.updateUserDetails(userName, userPassword);
               
        return message;
        
    }
    
    //Delete User
    @ResponseBody
    @RequestMapping(value="/admin/deleteuser/{userName}",method=RequestMethod.POST)
    public String deleteUser(@PathVariable String userName){
        String message = userName + " Has been terminated !!!";
        userDetailsService.deleteUserDetails(userName);
        return message;
    }
    
    //Add New User 
    @ResponseBody
    @RequestMapping(value="/admin/adduser/{userName}/{userPassword}/{userRole}",method= RequestMethod.POST)
    public String addUser(@PathVariable String userName,@PathVariable String userPassword,@PathVariable String userRole){
        String message = "User with user name '"+userName+"' has been added.";
        userDetailsService.saveUserDetails(userName, userPassword, userRole);
        return message;              
    }
    
    
    
}
