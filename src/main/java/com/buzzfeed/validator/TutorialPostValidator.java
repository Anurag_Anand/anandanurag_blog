/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.validator;

import com.buzzfeed.models.TutorialPost;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Anurag Anand
 */
public class TutorialPostValidator implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return TutorialPost.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "required.title", "Title of Post Can Not Be Empty");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "required.title", "Title of Post Can Not Be Empty");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfCreation", "required.dateOfCreation", "Date Of Creation Can Not Be Empty");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfModification", "required.dateOfModification", "Date Of Modification Can Not Be Empty");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postContent", "required.postContent", "Post Can Not Be Empty");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postUrlIdentificationTag", "required.postUrlIdentificationTag", 
//                                                          "Post Url Identification Tag Can Not Be Empty");
    }
    
}
