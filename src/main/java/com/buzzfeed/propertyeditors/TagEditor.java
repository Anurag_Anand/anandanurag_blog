/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.propertyeditors;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

/**
 *
 * @author Anurag Anand
 */

public class TagEditor extends CustomCollectionEditor {
private Log logger = LogFactory.getLog(getClass());
    
    
    public TagEditor(Class<? extends Collection> collectionType, boolean nullAsEmptyCollection) {
        super(collectionType, nullAsEmptyCollection);
    }
    
    @Override
    public void setAsText(String values){
        logger.info("Received value is "+values);
        Set<String> tags= new HashSet<>();
        if(null == values){            
            return;
        }else{
            String[] tagsValue = values.split("#");
            logger.info("Total Number of tags are "+tagsValue.length);            
            try{
            for(String tag : tagsValue){
                logger.info("Tags is "+tag);
                tags.add(tag);
                
            }
            }catch(Exception ex){
                logger.warn("Exception occured "+ex.getMessage(),ex);
            }
        }
        setValue(tags);
    }
    
    @Override
    public String getAsText(){
    //    String tagStr="";
    //    System.out.println("editor this.getValue "+ this.getSource() );
        
//        Set<String> tags = this.getValue();
//        for(String tag : tags){
//            System.out.println("Tag is "+tag);
//            tagStr += tag+"#";
//        }
//        
        return "";// tagStr.length()< 2 ? tagStr :  tagStr.substring(0,tagStr.length()-2);
    }
    
}
