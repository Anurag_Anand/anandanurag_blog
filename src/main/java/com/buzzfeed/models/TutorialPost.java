/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Type;

/**
 *
 * @author Anurag Anand
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE,region = "com.buzzfeed.models.TutorialPost")
@Table(name="TutorialPost")
@NamedQuery(name = "delete.post"
        ,query = "from TutorialPost post where post.postUrlIdentificationTag  = :postUrlIdentificationTag")
public class TutorialPost implements Serializable{
    private static final long serialVersionUID = 1L;
        
    private Long id;
    
    private String title;
    
    private Date dateOfCreation;
    
    private Date dateOfModification;

    private String postContent;
           
    private String postUrlIdentificationTag;    
    
    private boolean publishPost;
    
    private String brief;
    
    private String subMenuId;
    
    private String keyword;
    
    /**
     * @return the title
     */
    @Column(name="PostTitle",nullable = false,unique = true)
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the dateOfCreation
     */
    @Column(name="PostCreationDate",updatable = false)
    @Temporal(TemporalType.DATE)   
    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    /**
     * @param dateOfCreation the dateOfCreation to set
     */
    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    /**
     * @return the dateOfModification
     */
    @Column(name="PostModificationDate")
    @Temporal(TemporalType.DATE)   
    public Date getDateOfModification() {
        return dateOfModification;
    }

    /**
     * @param dateOfModification the dateOfModification to set
     */
    public void setDateOfModification(Date dateOfModification) {
        this.dateOfModification = dateOfModification;
    }

    /**
     * @return the postContent
     */
    @Column(name="PostContent",columnDefinition = "TEXT")
    public String getPostContent() {
        return postContent;
    }

    /**
     * @param postContent the postContent to set
     */
    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    /**
     * @return the tags
     */
     /* Since this mapping is not for Entity type so we need to use @ElementCollection Annotaion.
    *  So we need to provide a Table name for these Tags , OR it will default to 
    *  concatenation of the name of the containing entity and the name of the collection attribute, 
    *  separated by underscore i.e TutorialPost_tags .
    */
//    @ElementCollection
//    @CollectionTable(name="PostTags",joinColumns = @JoinColumn(name="Post_Id"))
//    @Column(name="Tags")
//    public Set<String> getTags() {        
//        return tags;
//    }

    /**
     * @param tags the tags to set
     */
//    public void setTags(Set<String> tags) {
//        this.tags = tags;
//    }

    /**
     * @return the postUrlIdentificationTag
     */    
    @Column(name="PostUrlIdentificationTag")
    public String getPostUrlIdentificationTag() {
        return postUrlIdentificationTag;
    }

    /**
     * @param postUrlIdentificationTag the postUrlIdentificationTag to set
     */
    public void setPostUrlIdentificationTag(String postUrlIdentificationTag) {
        this.postUrlIdentificationTag = postUrlIdentificationTag;
    }

    /**
     * @return the allowComments
     */
//    
//    @Column(name="AllowComments")
//    @Type(type="true_false")
//    public boolean isAllowComments() {
//        return allowComments;
//    }

    /**
     * @param allowComments the allowComments to set
     */
//    public void setAllowComments(boolean allowComments) {
//        this.allowComments = allowComments;
//    }
//  
    /**
     * @return the commentsPreview
     */
//     @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "tutorialPost",
//             targetEntity = CommentsPreview.class)
//    public Set<CommentsPreview> getCommentsPreview() {
//        return commentsPreview;
//    }

    /**
     * @param commentsPreview the commentsPreview to set
     */
//    public void setCommentsPreview(Set<CommentsPreview> commentsPreview) {
//        this.commentsPreview = commentsPreview;
//    }

    /**
     * @return the tutorialUniqueId
     */
    @Id
    @GeneratedValue(generator="tutorial_sequence",strategy=GenerationType.SEQUENCE)
    @SequenceGenerator(name="tutorial_sequence",sequenceName = "Tutorial_Sequence",initialValue = 10000)
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the brief
     */
    @Column(name="Brief")
    public String getBrief() {
        return brief;
    }

    /**
     * @param brief
     */
    public void setBrief(String brief) {
        this.brief = brief;
    }

    /**
     * @return the publishPost
     */
    @Column(name="PublishPost")
    @Type(type="true_false")
    public boolean isPublishPost() {
        return publishPost;
    }

    /**
     * @param publishPost the publishPost to set
     */
    public void setPublishPost(boolean publishPost) {
        this.publishPost = publishPost;
    }

    /**
     * @return the subMenuName
     */
    @Column(name="subMenuId",nullable = false)
    public String getsubMenuId() {
        return subMenuId;
    }

    /**
     * @param subMenuId the subMenuName to set
     */
    public void setsubMenuId(String subMenuId) {
        this.subMenuId = subMenuId;
    }

    /**
     * @return the keyword
     */
    @Column(name="keyword")
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
}
