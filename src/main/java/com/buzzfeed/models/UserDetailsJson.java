/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

/**
 *
 * @author Anurag Anand
 */
public class UserDetailsJson {
    private String userName;
    private String userRole;
    private boolean userEnabled;

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userRole
     */
    public String getUserRole() {
        return userRole;
    }

    /**
     * @param userRole the userRole to set
     */
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    /**
     * @return the userEnabled
     */
    public boolean getUserEnabled() {
        return userEnabled;
    }

    /**
     * @param userEnabled the userEnabled to set
     */
    public void setUserEnabled(boolean userEnabled) {
        this.userEnabled = userEnabled;
    }
}
