/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author Anurag Anand
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE,region = "com.buzzfeed.models.SubMenu")
public class SubMenu extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String name;
    
    private Menu menu;
    
    private String subMenuURL;
    
    private String subMenuTitle;
    
    private String subMenuDescription;
    
    private String subMenuKeywords;

    /**
     * @return the name
     */
    @Column(name="SubMenuName", nullable = false,unique = true)
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the menu
     */
    @ManyToOne
    @JoinColumn(name="MenuRef")
    public Menu getMenu() {
        return menu;
    }

    /**
     * @param menu the menu to set
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * @return the subMenuURL
     */
    @Column(name="SubMenuURL",length = 100)
    public String getSubMenuURL() {
        return subMenuURL;
    }

    /**
     * @param subMenuURL the subMenuURL to set
     */
    public void setSubMenuURL(String subMenuURL) {
        this.subMenuURL = subMenuURL;
    }

    /**
     * @return the subMenuTitle
     */
    @Column(name="SubMenuTitle",length=100)
    public String getSubMenuTitle() {
        return subMenuTitle;
    }

    /**
     * @param subMenuTitle the subMenuTitle to set
     */
    public void setSubMenuTitle(String subMenuTitle) {
        this.subMenuTitle = subMenuTitle;
    }

    /**
     * @return the subMenuDescription
     */
    @Column(name="SubMenuDescription",columnDefinition = "Text")
    public String getSubMenuDescription() {
        return subMenuDescription;
    }

    /**
     * @param subMenuDescription the subMenuDescription to set
     */
    public void setSubMenuDescription(String subMenuDescription) {
        this.subMenuDescription = subMenuDescription;
    }

    /**
     * @return the subMenuKeywords
     */
    @Column(name="SubMenuKeywords")
    public String getSubMenuKeywords() {
        return subMenuKeywords;
    }

    /**
     * @param subMenuKeywords the subMenuKeywords to set
     */
    public void setSubMenuKeywords(String subMenuKeywords) {
        this.subMenuKeywords = subMenuKeywords;
    }
    
    
    
}
