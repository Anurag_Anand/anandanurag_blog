/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Anurag Anand
 */
@Entity
@Table(name="UserRole")
public class UserRole extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String roleName;
    private List<UserDetails> userDetails;

    /**
     * @return the roleName
     */
    @Column(name="RoleName",nullable = false,unique = true)
    public String getRoleName() {
        return roleName;
    }

    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return the userDetails
     */
    @ManyToMany
    @JoinTable(name = "UserRoleMap",joinColumns = {@JoinColumn(name = "Role_ID")}
    , inverseJoinColumns = {@JoinColumn(name = "User_ID")})    
    public List<UserDetails> getUserDetails() {
        if(userDetails == null){
          userDetails =  new ArrayList<>();
        }
        return Collections.unmodifiableList(userDetails);
    }

    /**
     * @param userDetails the userDetails to set
     */
    public void setUserDetails(List<UserDetails> userDetails) {
        this.userDetails = userDetails;
    }
}
