/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author Anurag Anand
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE,region = "com.buzzfeed.models.Menu")
public class Menu extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String name;
    
    private Set<SubMenu> subMenu;

    /**
     * @return the name
     */
    @Column(name="MenuName", nullable = false,unique = true)
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the subMenu
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,targetEntity = SubMenu.class
    ,fetch = FetchType.EAGER,mappedBy = "menu")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public Set<SubMenu> getSubMenu() {
        if(subMenu == null){
            return Collections.<SubMenu>emptySet();
        }
        return subMenu;
    }

    /**
     * @param subMenu the subMenu to set
     */
    public void setSubMenu(Set<SubMenu> subMenu) {
        this.subMenu = subMenu;
    }
    
    
}
