/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 *
 * @author Anurag Anand
 */
@Entity
@Table(name="CommentPreview")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE,
        region = "com.buzzfeed.models.CommentsPreview")
public class CommentsPreview implements Serializable{
    private static final long serialVersionUID = 1L;
        
    private Long uniqueCommentId;
       
    private String comment;
       
    private String commentDateAndTime;
        
    private String commentorName;
        
    private String email;
    
    private TutorialPost tutorialPost;
    
    private boolean isCommentVisible;
    /**
     * @return the comment
     */
     @Column(name="comment",nullable = false)
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the commentDateAndTime
     */
     @Column(name="CommentDateTime",nullable = false)
    public String getCommentDateAndTime() {
        return commentDateAndTime;
    }

    /**
     * @param commentDateAndTime the commentDateAndTime to set
     */
    public void setCommentDateAndTime(String commentDateAndTime) {
        this.commentDateAndTime = commentDateAndTime;
    }
      
    /**
     * @return the commentorName
     */
    @Column(name="CommentedBy",nullable = false)
    public String getCommentorName() {
        return commentorName;
    }

    /**
     * @param commentorName the commentorName to set
     */
    public void setCommentorName(String commentorName) {
        this.commentorName = commentorName;
    }

    /**
     * @return the email
     */
    @Column(name="EmailId")
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the isCommentVisible
     */
    @Column(name="IsCommentVisible",nullable = false)
    @Type(type="true_false")
    public boolean isIsCommentVisible() {
        return isCommentVisible;
    }

    /**
     * @param isCommentVisible the isCommentVisible to set
     */
    public void setIsCommentVisible(boolean isCommentVisible) {
        this.isCommentVisible = isCommentVisible;
    }

    /**
     * @return the tutorialPost
     */
    @ManyToOne
    @JoinColumn(name="Tutorial_Id")
    public TutorialPost getTutorialPost() {
        return tutorialPost;
    }

    /**
     * @param tutorialPost the tutorialPost to set
     */
    public void setTutorialPost(TutorialPost tutorialPost) {
        this.tutorialPost = tutorialPost;
    }

    /**
     * @return the uniqueCommentId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Comment_Sequence")
    @SequenceGenerator(name="Comment_Sequence",sequenceName = "Comment_Sequence",initialValue = 10000)
    public Long getUniqueCommentId() {
        return uniqueCommentId;
    }

    /**
     * @param uniqueCommentId the uniqueCommentId to set
     */
    public void setUniqueCommentId(Long uniqueCommentId) {
        this.uniqueCommentId = uniqueCommentId;
    }
}
