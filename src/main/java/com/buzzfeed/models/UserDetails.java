/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author Anurag Anand
 */
@Entity
@Table(name = "UserDetails")
public class UserDetails extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;
    private String userName;
    private String password;
    private List<UserRole> userRoles;
    private boolean enabled;

    /**
     * @return the userName
     */
    @Column(name="UserName",nullable = false,unique = true)
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    @Column(name="Password",nullable = false)
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userRoles
     */
    @ManyToMany(cascade = CascadeType.ALL
            ,targetEntity = UserRole.class,mappedBy = "userDetails") 
    public List<UserRole> getUserRoles() {
        if(null == userRoles){
         userRoles =   new ArrayList<>();
        }
        
        return Collections.unmodifiableList(userRoles);
    }

    /**
     * @param userRoles the userRoles to set
     */
    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    /**
     * @return the enabled
     */
    @Column(name = "Enabled")
    @Type(type="true_false")
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
