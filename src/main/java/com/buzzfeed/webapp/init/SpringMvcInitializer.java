/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzfeed.webapp.init;

import com.buzzfeed.configs.BuzzFeedConfig;
import com.buzzfeed.configs.DataSourceConfig;
import com.buzzfeed.configs.SecurityConfig;
import com.buzzfeed.configs.SpringSecurityInitializer;
import com.buzzfeed.listeners.SessionListener;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
/**
 *
 * @author Anurag Anand
 */
@Configuration
@ComponentScan("com.buzzfeed.configs")
public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
//public class SpringMvcInitializer implements WebApplicationInitializer  {
  /*  
     @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);
        
        AnnotationConfigWebApplicationContext context    = new AnnotationConfigWebApplicationContext();
        context.setServletContext(servletContext);
        
        servletContext.addListener(new ContextLoaderListener(context));
        servletContext.addServlet("buzzfeed", new DispatcherServlet().getClass());
       
        FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encodingFilter", new CharacterEncodingFilter());
        encodingFilter.setInitParameter("encoding", "UTF-8");
        encodingFilter.setInitParameter("forceEncoding", "true");



    }
    */
//    @Override
//    public void onStartup(ServletContext servletContext) throws ServletException {
//        WebApplicationContext context = getContext();
//        servletContext.addListener(new ContextLoaderListener(context));
//        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
//       FilterRegistration.Dynamic securityFilter = servletContext.addFilter("springSecurityFilterChain", DelegatingFilterProxy.class);
//       if(null == securityFilter ){
//           System.out.println("security Filter is NULL");
//       }
//        securityFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/admin/**");
//     //  securityFilter.
//        dispatcher.setLoadOnStartup(1);
//        dispatcher.addMapping("/");
//    }
//
//    private AnnotationConfigWebApplicationContext getContext() {
//        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//        context.setConfigLocation("com.buzzfeed.config");
//        return context;
//    }
//   
    
    @Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { DataSourceConfig.class,
                    SpringSecurityInitializer.class,SecurityConfig.class};
	}
 
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { BuzzFeedConfig.class};// ,SecurityConfig.class};
	}
 
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
       
        
        @Override
        public void onStartup(ServletContext servletContext) throws ServletException{
            super.onStartup(servletContext);
            servletContext.addListener(new SessionListener());
        }
        

//    @Override
//    protected Class<?>[] getRootConfigClasses() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    protected Class<?>[] getServletConfigClasses() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    protected String[] getServletMappings() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
