<%-- 
    Document   : CreateMenu
    Created on : Apr 25, 2015, 1:48:33 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"  session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=500, initial-scale=1">
        <title>Create Menu</title>
    </head>             
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <body>
            <%@include file="../jspf/admin-header.jspf" %>
        
            <div class="container-fluid">
                <div class="page-header">
                    Create Menu
                </div>
                <c:url value="save-menu" var="menuUrl" ></c:url>
                <form:form commandName="menu" method="get" action="${menuUrl}">
                <div class="form-group">
                    <form:label path="name" cssClass="col-sm-2 control-label">Menu Name</form:label>
                    <div class="col-sm-10">
                    <form:input cssClass="form-control" path="name" ></form:input>                                                
                    </div>
                    <form:errors  cssClass="error"></form:errors>  
                </div>

                <div class="form-group">
                       <div class="col-sm-offset-2 col-sm-10">
                            <input class="btn btn-primary btn-lg" type="Submit" value="New Menu" /> 
                       </div>
                </div>  
            
            </form:form>           
                       
             </div>
            <%@include file="../jspf/footer.jspf" %>
    </body>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" >
    //window.domainRoot = '@(Url.Content("~/"))';
    root = location.protocol + '//' + location.host;
</script>
<script src="${pageContext.servletContext.contextPath}/resources/js/admin.js" type="text/javascript"></script>
  
</html>
