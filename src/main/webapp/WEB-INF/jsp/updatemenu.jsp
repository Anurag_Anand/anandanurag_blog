<%-- 
    Document   : updatemenu
    Created on : Apr 25, 2015, 10:12:22 PM
    Author     : Anurag Anand
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:url value="../update-menu/${menu.id}" var="updateUrl" ></c:url>
        <form action="${updateUrl}" method="get" >
            <input type="text" name="name" value="${menu.name}" />
            <input type="submit" name="submit" value="Submit" />
        </form>
    </body>
</html>
