<%-- 
    Document   : viewAllPost
    Created on : Apr 12, 2015, 11:21:04 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Posts</title>
        <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="${pageContext.servletContext.contextPath}/resources/css/dashboard.min.css" rel="stylesheet">
    </head>
    <body>
        <%@include file="../jspf/list-post-header.jspf" %>
        
        <div class="container">
            <div class="row" >
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                <ul class="list-group ">
                    <li class="list-group-item">Posts <span class="badge"> ${posts.size()} </span> </li>
                </ul>
                </div>
            </div>
        <table id="list-of-post" class="table table-striped table-responsive"> 
            <tr>
                <th>Title</th>
                <th>Date Of Creation</th>
                <th>Date Of Modification</th>
                <th>Post ID</th>
                <th>Edit Post</th>
                <th>Delete Post</th>
            </tr>
        <c:forEach items="${posts}" var="post" varStatus="count" >
            <tr>
                <td>
                    <c:url value="/admin/preview-post/${post.postUrlIdentificationTag}" var="previewPostUrl"></c:url>
                    <a href="${previewPostUrl}"> ${post.title} </a>
                </td> 
                <td>${post.dateOfCreation}</td> 
                <td>${post.dateOfModification}</td> 
                <td>${post.postUrlIdentificationTag}</td> 
                <td>
                    <c:url value="/admin/edit-post/${post.postUrlIdentificationTag}" var="editPostUrl"></c:url>
                    <a href="${editPostUrl}" >edit</a>
                </td> 
                <td>
                    <c:url value="/admin/delete-post/${post.postUrlIdentificationTag}" var="deletePostUrl"></c:url>
                    <a href="${deletePostUrl}" >delete</a>
                </td> 
            </tr>
        </c:forEach>
        </table>
            
    </div>
<%@include file="../jspf/footer.jspf" %>
    </body>
           

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script src="${pageContext.servletContext.contextPath}/resources/js/buzzfeed.js" type="text/javascript"></script>

</html>
