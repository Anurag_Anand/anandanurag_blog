<%-- 
    Document   : viewpost
    Created on : Apr 12, 2015, 11:22:03 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width" name="viewport" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="${post.brief}">
    <meta name="author" content="Anurag Anand">
    <meta name="keywords" content="${post.keyword}">
<meta name="twitter:site" content="@anurag789786" />
<meta  property="og:title" name="twitter:title" content="${post.title}" />
<meta itemprop="description" property="og:description"  name="twitter:description" content="${post.brief}" />
<meta property="og:url" name="twitter:url" content="${requestScope['javax.servlet.forward.request_uri']}" />

    <title>Preview</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/viewpost.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!--Page Header -->
<%@include file="../jspf/admin-header.jspf" %>
  
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <c:url value="/admin/edit-post/${post.postUrlIdentificationTag}" var="editPostUrl"></c:url>
                 <a href="${editPostUrl}" class="btn btn-success">Edit current post</a>
            </div>
        </div>
        
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h3>${post.title}</h3>

                <!-- Author -->
                <p class="lead">
                    <small>  by <a href="#">Anurag Anand</a> </small>
                </p>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on ${post.dateOfCreation}</p>

                <hr>


                <!-- Post Content -->
                <p class="tutorial-post" data-id="${post.id}" id="tutorial-post">
                    ${post.postContent}
                </p>

                <hr>
            </div>
        <!-- /.row -->

        

    </div>
    </div>
    <!-- /.container -->
    <%@include file="../jspf/footer.jspf" %>
</body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" >
    //window.domainRoot = '@(Url.Content("~/"))';
    root = location.protocol + '//' + location.host;
</script>
<script src="${pageContext.servletContext.contextPath}/resources/js/admin.js" type="text/javascript"></script>


</html>
