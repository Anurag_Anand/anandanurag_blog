<%-- 
    Document   : createsubmenu
    Created on : Apr 25, 2015, 10:40:37 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
        <c:url value="../update-submenu/${submenuid}" var="subMenuUrl" ></c:url>
        <form action="${subMenuUrl}" method="get">
            <div class="form-group">
                <input class="col-lg-10" type="text" name="submenuName" value="${submenu.name}" id="name" placeholder="SubMenuName" />
            </div>
            
            <div class="form-group">
                <input class="col-lg-10" type="text" name="subMenuURL" value="${submenu.subMenuURL}" id="name" placeholder="subMenuURL" />
            </div>
            
            <div class="form-group">
                <input class="col-lg-10" type="text" name="subMenuTitle" value="${submenu.subMenuTitle}" id="name"
                       placeholder="subMenuTitle"  />
            </div>
            
            <div class="form-group">
                <input class="col-lg-10" type="text" name="subMenuDescription" value="${submenu.subMenuDescription}" id="name" 
                       placeholder="subMenuDescription"/>
            </div>
            
            <div class="form-group">
                <input class="col-lg-10" placeholder="subMenuKeywords" type="text" name="subMenuKeywords" value="${submenu.subMenuKeywords}" id="name" />
            </div>
            
            <div class="form-group">
                   <div class="col-sm-offset-2 col-sm-10">
                        <input class="btn btn-primary btn-lg" type="Submit" value="Update SubMenu" /> 
                   </div>
            </div> 
            
            </form>
            </div>
    </body>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/buzzfeed.js" type="text/javascript"></script>
      
</html>
