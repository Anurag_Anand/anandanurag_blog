<%-- 
    Document   : editPost
    Created on : Apr 12, 2015, 11:21:23 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=500, initial-scale=1">
        <title>${post.title}</title>  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link href="${pageContext.servletContext.contextPath}/resources/css/dashboard.min.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/editor.min.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/viewpost.min.css" rel="stylesheet">

    </head>
    <body>
        
        <%@include file="../jspf/list-post-header.jspf" %>
        
        <!-- Creating New Posts using Spring form tag -->
        <div class="container-fluid"> <div class="container-fluid ">
   <!-- Adding Accordion -->       
   <div class="panel-group" id="post_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#post_accordion" href="#post_details" aria-expanded="true" aria-controls="collapseOne">
          Tutorial Post Details ...
        </a>
      </h4>
    </div>
    <div id="post_details" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
<!-- Accordion Body Starts -->        
        <c:url value="../update-post"  var="actionUrl"/>
        <form:form commandName="post" method="post" action="${actionUrl}" cssClass="form-horizontal">
            <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}" />
            <div class="row">
                <div class="form-group">
                    <div class="col col-lg-2">
                    <form:label path="title" cssClass="control-label">Post Title</form:label>
                    </div>
                    <div class="col col-lg-10">
                            <form:input cssClass="form-control" path="title" ></form:input> 
                            <form:errors  cssClass="error"></form:errors>  
                    </div>
                </div>
        </div>
              <div class="row">
                <div class="form-group">
                    <div class="col col-lg-2">
                    <form:label path="postUrlIdentificationTag"
                                cssClass="control-label">Unique URL Identification </form:label>                         
                    </div>
                   <div class="col col-lg-10">    
                <form:input cssClass="form-control" path="postUrlIdentificationTag" ></form:input>
                    <form:errors  cssClass="error"></form:errors>
                    </div>
                </div>
              </div>    
                    <div class="row">
                <div class="form-group">
                    <div class="col col-lg-2">
                    <form:label path="brief"
                                cssClass="control-label">A brief description </form:label>                         
                    </div>
                    <div class="col col-lg-10">    
                        <form:input cssClass="form-control" path="brief" ></form:input>
                        <form:errors  cssClass="error"></form:errors>
                    </div>
                </div>
                    </div>
                    <div class="row">
                    <div class="form-group">
                        <div class="col col-lg-2">
                            <form:label path="keyword"
                                cssClass=" control-label">Keywords </form:label>                         
                        </div>
                    <div class="col col-lg-10">    
                        <form:input cssClass="form-control" path="keyword" ></form:input>
                        <form:errors  cssClass="error"></form:errors>
                    </div>
                </div>
                </div>
                    
                <div class="row">    
                <div class="form-group">
                    <div class="col col-lg-2">
                    <form:label path="subMenuId"
                                cssClass="control-label">Select sub-Menu</form:label>                         
                    </div>
                    <div class="col col-lg-4">    
                        
                    <form:select items="${submenu}"  path="subMenuId">
                        
                        <!--form:options  / -->
                    </form:select>
                        
                        <form:errors  cssClass="error"></form:errors>
                    </div>
                
                      <div class="col col-lg-2">
                         <form:label path="publishPost" cssClass="control-label">Publish Post</form:label> 
                        </div>
                         <div class="col-lg-1 checkbox">
                             <form:checkbox cssClass="form-control" path="publishPost" ></form:checkbox>
                             <form:errors  cssClass="error"></form:errors>
                         </div>
                        </div>
                </div>                                             
    <!-- Accordion Body ends -->                     
        </div>
        </div>
        </div>               
                    <!-- HTML Editor -->
                    <%@include file="../jspf/editor.jspf" %>
                    
               <div class="form-group">
                   <div class="col-sm-offset-2 col-sm-10">
                        <input class="btn btn-primary btn-lg" type="Submit" value="Update Post" /> 
                   </div>
               </div>       
        </form:form>
        </div> <!--Body Container -->
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/buzzfeed.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jquery.selection.js" ></script>       
<script src="${pageContext.servletContext.contextPath}/resources/js/editor.js" ></script>
</html>
