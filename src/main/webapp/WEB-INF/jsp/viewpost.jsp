<%-- 
    Document   : viewpost
    Created on : Apr 12, 2015, 11:22:03 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="${post.brief}">
    <meta name="author" content="Anurag Anand">
    <meta name="keywords" content="${post.keyword}">
    
    <!-- Twitter card -->
    <%@include file="../jspf/twitter-card.jspf"  %>
    
    <!-- Facebook card -->
    <%@include file="../jspf/opengraph.jspf"  %>
    
    <title>${post.title}</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/viewpost.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!--Page Header -->
<%@include file="../jspf/header.jspf" %>
  
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h3>${post.title}</h3>

                <!-- Author -->
                <!-- Facebook Like and share button -->
            
            <!--Fb like and share button ends -->
                <p class="">
                   <small> <span class="glyphicon glyphicon-time"></span> Posted on ${post.dateOfCreation}
                         by <a href="http://www.anandanurag.com">Anurag Anand</a></small>
                    <div
                        class="fb-like"
                        data-share="true"
                        data-width="450"
                        data-show-faces="false">
                    </div>
                </p>

                <hr>
                <!-- Post Content -->
                <p class="tutorial-post" data-id="${post.id}" id="tutorial-post">
                    ${post.postContent}
                </p>

                <hr>       
              <!-- Load DisQus Here-->
              <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'anandanurag';
            var disqus_identifier = '${post.postUrlIdentificationTag}';
            var disqus_title = '${post.title}';
            var disqus_url = 'http://www.anandanurag.com/tutorial/${post.postUrlIdentificationTag}';
        //    alert(disqus_url);
            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                        </div>

        <!-- /.row -->
        </div>
        <hr>

        

    </div>
    <!-- /.container -->
<%@include file="../jspf/footer.jspf" %>
</body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" >
    root = location.protocol + '//' + location.host;
</script>


</html>
