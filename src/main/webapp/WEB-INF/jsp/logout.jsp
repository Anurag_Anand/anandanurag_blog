<%-- 
    Document   : logout
    Created on : Apr 12, 2015, 2:21:11 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>You have been successfully logged out!</h1>
        <c:url value="/login" var="loginUrl" />
        <a href="${loginUrl}">Login</a>
    </body>
</html>
