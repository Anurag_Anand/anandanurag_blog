<%-- 
    Document   : createsubmenu
    Created on : Apr 25, 2015, 10:40:37 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"  session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create SubMenu</title>
             
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        <%@include file="../jspf/admin-header.jspf" %>
        
        <c:url value="../save-submenu/${menuid}" var="subMenuUrl" ></c:url>
        <div class="container">
            <form action="${subMenuUrl}" method="get">
            <div class="form-group col-lg-4">
                <input class="form-control" type="text" name="submenuName" value="" id="submenu" placeholder="SubMenuName"/>
            </div>
            <div class="form-group col-lg-4">
                <input class="form-control" type="text" name="subMenuURL" value="" id="submenu" placeholder="SubMenuUrl"/>
            </div>
            <div class="form-group col-lg-4">
                <input class="form-control" type="text" name="subMenuTitle" value="" id="submenu" placeholder="SubMenuTitle"/>
            </div>
            <div class="form-group  col-lg-4">
                <input class="form-control" type="text" name="subMenuDescription" value="" id="submenu" placeholder="SubMenuDescription"/>
            </div>
            
            <div class="form-group col-lg-4">
                <input class="form-control" type="text" name="subMenuKeywords" value="" id="submenu" placeholder="SubMenuKeywords"/>
            </div>            
            
            <div class="form-group col-lg-4">
                   <div class="col-sm-offset-2 col-sm-10">
                        <input class="btn btn-primary btn-lg" type="Submit" value="Create SubMenu" /> 
                   </div>
            </div> 
             </form>
        </div>
           
   <%@include file="../jspf/footer.jspf" %>
    </body>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" >
    //window.domainRoot = '@(Url.Content("~/"))';
    root = location.protocol + '//' + location.host;
</script>
<script src="${pageContext.servletContext.contextPath}/resources/js/admin.js" type="text/javascript"></script>
  
</html>
