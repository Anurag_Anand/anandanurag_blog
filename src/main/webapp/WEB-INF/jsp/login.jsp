<%-- 
    Document   : login
    Created on : Apr 12, 2015, 10:12:03 AM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>     
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link href="${pageContext.servletContext.contextPath}/resources/css/login.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body onload="document.loginForm.username.focus()">
<div class="container-fluid pageGradient">
<div class="row">
	<div class="col col-lg-8 col-md-8 col-xs-12
	 col-lg-offset-3 col-md-offset-3 col-sm-offset-2
	 vertical-offset-top-3">
		<span class="custom-text"> Anurag Anand</span>
	</div>
</div>
	<div class="row">
		<div class="col col-lg-4 col-md-8 col-xs-12
		col-lg-offset-4 col-md-offset-4 col-sm-offset-3 vertical-offset-top-4">
		 <form autocomplete="off" name="loginForm" method="post" 
                    action="${j_spring_security_check}" >
			<div class="materialize-group">
                            <input name="username" class="custom-input" type="text"  tabindex="10" value="" required/>
				<span class="underline"></span>
				<label class="placeholder">User Name</label>
			</div>
			<div class="materialize-group">
                            <input name="password" class="custom-input" type="password" tabindex="11" value="" required/>
				<span class="underline"></span>		
				<label class="placeholder">Password</label>		
			</div>
                        <input type="submit" class="custom-button" tabindex="12" Value="Login"/>
                         <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}" />
                 </form>
			
		</div>
	</div>
</div> 
            
        </div-->
    </body>
</html>
