<%-- 
    Document   : viewpost
    Created on : Apr 12, 2015, 11:22:03 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width" name="viewport" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="${submenu.subMenuDescription}">
    <meta name="keywords" content="${submenu.subMenuKeywords}">
    <meta name="author" content="Anurag Anand">
    
     <!-- Twitter card -->
    <%@include file="../jspf/twitter-card_menu.jspf"  %>
    
    <!-- Facebook card -->
    <%@include file="../jspf/opengraph_menu.jspf"  %>
    
    <title>Home</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/viewpost.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body> 
        <!--Page Header -->
<%@include file="../jspf/header.jspf" %>
    
    
    
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <!-- Facebook Like and share button -->
            

            <!--Fb like and share button ends -->
            <!-- Blog Post Content Column -->
            <div class="col-lg-8">
                <div
  class="fb-like"
  data-share="true"
  data-width="450"
  data-show-faces="false">
</div>
                <ul class="list-unstyled">
                  <c:forEach items="${posts}" var="post" varStatus="count" >
                    <li>
                        <h4><a href="/tutorial/${post.postUrlIdentificationTag}"  title="${post.title}">${post.title}</a> </h4> 
                        <h5>${post.brief}</h5>
                    </li>
                    <li>
                        <h6>Created On :- ${post.dateOfCreation}</h6> 
                        <hr />
                    </li>

                  </c:forEach>
                </ul>
                           
                <hr>
               

            </div>
           

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->
        <%@include file="../jspf/footer.jspf" %>

</body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script src="${pageContext.servletContext.contextPath}/resources/js/buzzfeed.js" type="text/javascript"></script>

</html>
