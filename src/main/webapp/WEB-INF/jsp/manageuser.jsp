<%-- 
    Document   : menu
    Created on : Apr 25, 2015, 9:20:22 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"  session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Users</title>
        
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="${pageContext.servletContext.contextPath}/resources/css/dashboard.min.css" rel="stylesheet">
    </head>
    <body>
        <%@include file="../jspf/admin-header.jspf" %>
        <div  class="container">
            <div class="row">
                <div class="col col-lg-5">
                    <div class="col">
                    <label for="username" class="label label-default">User-Name</label>
                    <input name="username" id="username" required> 
                    </div>
                    <div class="col">
                    <label for="newPassword" class="label label-default">New Password</label>
                    <input type="Password" name="newPassword" id="newPassword" required> 
                    </div>
                    <div class="col">
                    <label for="confirmPassword" class="label label-default">Confirm Password</label>
                    <input type="Password" name="confirmPassword" id="confirmPassword" required> 
                    </div>
                    <div class="col">
                    <label for="userrole" class="label label-default">User Role</label>
                    <input name="userrole" id="userrole" required> 
                    </div>
                    <div class="col">
                        <input id="csrftoken" type="hidden" data-name="${_csrf.parameterName}"
                               name="${_csrf.parameterName}"
                           value="${_csrf.token}" />
                        <button type="button" name="add_user" id="add_user" >Add User</button>
                    </div>
                </div>
                
                <div class="col col-lg-5">
                    <table class="table table-striped"id="userListTable">
                        <thead>
                            <tr><th>User Name</th> <th>New Password</th> <th>Update</th> <th>Delete</th> </tr>
                        </thead> 
                        <tbody id="userlist">
                            <c:forEach items="${users}" var="user" varStatus="userIndex">
                                <tr>
                                    <td>${user.userName}</td>
                                    <td>
                                        <input type="password" name="password" id="${user.userName}" >
                                    </td>
                                    <td>
                                        <button type="Button" class="btn btn-default update-user"  name="updateuser" data-user="${user.userName}">Update</button>
                                    </td>
                                    <td>
                                        <button type="Button" class="btn btn-default delete-user" name="updateuser" data-user="${user.userName}">Delete</button>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                <%@include file="../jspf/footer.jspf" %>
    </body>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" >
    //window.domainRoot = '@(Url.Content("~/"))';
    root = location.protocol + '//' + location.host;
</script>
<script src="${pageContext.servletContext.contextPath}/resources/js/admin.js" type="text/javascript"></script>


</html>
