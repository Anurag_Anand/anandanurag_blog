<%-- 
    Document   : menu
    Created on : Apr 25, 2015, 9:20:22 PM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"  session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu List</title>
        
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="${pageContext.servletContext.contextPath}/resources/css/dashboard.min.css" rel="stylesheet">
    </head>
    <body>
        <%@include file="../jspf/admin-header.jspf" %>
        <div  class="container">
        <ul class="list-inline list-group">

            <li class="list-group-item">
                    <c:url value="new-menu" var="newMenuUrl"></c:url>
                    <a href="${newMenuUrl}" >New Menu</a>
                </li>
                <li class="list-group-item">
                    <c:url value="new-submenu" var="newSubMenuUrl"></c:url>
                    <a href="${newSubMenuUrl}" >New Sub-Menu</a>
                </li>
            
        </ul>
        <table id="list-of-menu" class="table table-responsive table-responsive">
        <c:forEach items="${allmenu}" var="menu" varStatus="count" >
            <tr>
                <td>${menu.name}</td> 
                
                <td>
                    <c:url value="new-submenu/${menu.id}" var="newMenuUrl"></c:url>
                    <a href="${newMenuUrl}" >New Sub-menu</a>
                </td> 
                <td>
                    <c:url value="edit-menu/${menu.id}" var="editMenuUrl"></c:url>
                    <a href="${editMenuUrl}" >Edit</a>
                </td> 
                <td>
                    <c:url value="delete-menu/${menu.id}" var="deleteMenuUrl"></c:url>
                    <a href="${deleteMenuUrl}" >Delete</a>
                </td> 
            </tr>
            <tr>
                <td>
                <table id="list-of-submenu" class="table table-bordered table-responsive">
                    <c:forEach items="${menu.subMenu}" var="submenu" varStatus="count" >
                        <tr>
                            <td>${submenu.name}</td>
                            <td>
                                <c:url value="edit-submenu/${submenu.id}" var="editSubMenuUrl"></c:url>
                                <a href="${editSubMenuUrl}" >Edit</a>
                            </td>
                            <td>
                                <c:url value="delete-submenu/${menu.id}/${submenu.id}" var="deleteSubMenuUrl"></c:url>
                                <a href="${deleteSubMenuUrl}" >Delete</a>
                            </td>                            
                        </tr>
                    </c:forEach>
                </table>
             </td>
            </tr>
            </c:forEach>
        </table>
                </div>
                <%@include file="../jspf/footer.jspf" %>
    </body>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" >
    //window.domainRoot = '@(Url.Content("~/"))';
    root = location.protocol + '//' + location.host;
</script>
<script src="${pageContext.servletContext.contextPath}/resources/js/admin.js" type="text/javascript"></script>


</html>
