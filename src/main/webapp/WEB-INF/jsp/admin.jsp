<%-- 
    Document   : admin
    Created on : Apr 12, 2015, 10:12:10 AM
    Author     : Anurag Anand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashboard</title>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/dashboard.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
        
        <%@include file="../jspf/admin-header.jspf" %>
       <!-- 
        <h1>Title :- ${title}</h1>
        <h1>Message :- ${message}</h1>
        -->
        <!-- c:url value="${logout}" var="logoutURL" ><!--/c:url -->
        <c:url value="/logout" var="logoutURL" />
        
        <form id="logoutForm" action="${logoutURL}" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>                   
        </form>
        <script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>
        
        <c:if test="${pageContext.request.userPrincipal.name != null}">
                <h4>
                    Welcome  ${pageContext.request.userPrincipal.name} | 
                    <a href="javascript:formSubmit()" >logout</a>
                </h4>
        </c:if>
            
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script src="${pageContext.servletContext.contextPath}/resources/js/buzzfeed.js" type="text/javascript"></script>

</html>
