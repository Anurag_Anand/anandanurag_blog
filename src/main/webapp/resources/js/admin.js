/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var globalAdminUrl = "/admin/"
var ajaxCallRunning = false

$(function(){    
   bindClickUsers(true)   
});


//Bind on click events on User controls
function bindClickUsers(firstTime){
    
    if(firstTime){
        $("#add_user").on("click",function(event){
            addUser(event)
       })
    }
   $(".update-user").on("click",function(event){
       //Active class is added to identify the currently clicked Button
      // $(event.target).addClass("active")
      updateUser(event.target)
   })
   $(".delete-user").on("click",function(event){
       //Active class is added to identify the currently clicked Button
      // $(event.target).addClass("active")
      deleteUser(event.target)
   })
   
}
//common function to make ajac calls
function makeAjaxCall(url_url,method_type,data){
    var ajaxPromise = $.ajax({
                method : method_type,
                url : url_url,
                data : data
            }
            )
    
    return ajaxPromise;
}

function updateUser(currentlyActive){        
    
    var userName = $(currentlyActive).data("user")
    var password = $("#"+userName).val()
    
    if(userName.length <= 0){
        alert("User Name is empty")
        return
    }
    
    if(password.length <= 0){
        alert("Password is empty")
        return
    }
    
    var updateUrl = globalAdminUrl+"updateuser/"+userName+"/"+password
    
    //set ajax call variable
    ajaxCallRunning = true
    
    var ajaxPromise = makeAjaxCall(updateUrl,"post",csrftoken())
    
    //set ajax call variable
    ajaxCallRunning = false
    
    //On success of ajax
    ajaxPromise.done(function(data,textStatus,jqXHR){
        alert(jqXHR.responseText)
    })
    
    //On failure of ajax
    ajaxPromise.fail(function(jqXHR, textStatus,errorThrownObj){
        alert(textStatus)
    })
    
    //remove .active class from that button
  //  $(".update-user.active").removeClass(".active")
}

//delete current user
function deleteUser(currentlyActive){
    
    var userName = $(currentlyActive).data("user")
    
    if(userName.length <= 0){
        alert("User Name is empty")
        return
    }
 
    
    var deleteUrl = globalAdminUrl+"deleteuser/"+userName
    
    //set ajax call variable
    ajaxCallRunning = true
    
    var ajaxPromise = makeAjaxCall(deleteUrl,"post",csrftoken())
    
    //set ajax call variable
    ajaxCallRunning = false
    
    //On completion of ajax
    ajaxPromise.done(function(data,textStatus,jqXHR){
        alert(jqXHR.responseText)
        listUsers()
    })
    
    //On failure of ajax
    ajaxPromise.fail(function(jqXHR, textStatus,errorThrownObj){
        alert(textStatus)
    })
}

//Render List of Users
function listUsers(){
    var url = globalAdminUrl+"getAllUser"
    var methodType = 'get'
    
    var ajaxPromise = makeAjaxCall(url,methodType,csrftoken())
    
    ajaxPromise.done(function(data,status,jqXHR){
        console.log(jqXHR.responseJSON)
        //add newly created users 
        updateUserTable(jqXHR.responseJSON)
        //Bind click events to newly created
        bindClickUsers(false)
    })
    
    //Ajax fail
   ajaxPromise.fail(function(jqXHR,status,errorThrown){
       alert(errorThrown+" "+status)
       console.log("Failed to load User list")
   })
}

//Clean Add user form
function clearAddUser(){
    $("#username").val("")
    $("#newPassword").val("")
    $("#confirmPassword").val("")
    $("#userrole").val("")
}

//ADD User
function addUser(event){
    var userName = $("#username").val()
    var userPassword = $("#newPassword").val()
    var userConfirmPassword = $("#confirmPassword").val()
    var userRole = $("#userrole").val()

    if(userName.length <= 0){
        alert("User name is empty")
        return
    }
    
    if(userPassword.length <= 0){
        alert("Password field is empty")
        return
    }
    
    if(userRole.length <= 0){
        alert("User Role is empty")
        return
    }
    
    if( ! (userConfirmPassword === userPassword)){
        alert("Password Do not match")
        return
    }
    
    var addUrl = globalAdminUrl+"adduser/"+userName+"/"+userPassword+"/"+userRole
    console.log("User add url "+addUrl)
   var ajaxPromise = makeAjaxCall(addUrl,'post',csrftoken())
   //Ajax Success
   ajaxPromise.done(function(data,status,jqXHR){
       alert(jqXHR.responseText)
       clearAddUser()
       listUsers()
   })
   //Ajax fail
   ajaxPromise.fail(function(jqXHR,status,errorThrown){
       alert(errorThrown+" "+status)
   })
}


//getCsrf token
function csrftoken(){
    var csrfName = $("#csrftoken").data("name")
    var csrf = {}
    
    var csrfValue = $("#csrftoken").val()
    
    csrf[csrfName.valueOf()] =  csrfValue
    return csrf
}

//Update user list table
function updateUserTable(jsonUserList){
    var $table = $("#userListTable")
    var $tbody = $("#userlist");
    $tbody.detach()
    $tbody.empty()
    
    var rows = ""
    
    $.each(jsonUserList,function(index,user){
        rows+="<tr>"        
        rows+="<td>"+user.userName+"</td>"
        +"<td><input type='password' name='password' id='"+user.userName+"' ></td>"
        + "<td><button type='Button' class='btn btn-default update-user'  name='updateuser' data-user='"+user.userName+"'>Update</button></td>"
        + "<td><button type='Button' class='btn btn-default delete-user' name='updateuser' data-user='"+user.userName+"'>Delete</button></td>"
        rows+="</tr>"
    })
    
    $tbody.append(rows)
    $table.append($tbody) 
}
