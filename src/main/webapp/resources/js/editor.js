var html_class_map = {}

//Text editing
html_class_map["Bold"] = {'dom' : "B",'attributes' : [],'class' : ""}
html_class_map["Italic"] =  {'dom' : "I",'attributes' : [],'class' : ""}
html_class_map["Underline"] =  {'dom' : "U",'attributes' : [],'class' : ""}
html_class_map["Strong"] =  {'dom' : "Strong",'attributes' : [],'class' : ""}
html_class_map["Strike"] =  {'dom' : "del",'attributes' : [],'class' : ""}
html_class_map["Mark"] =  {'dom' : "mark",'attributes' : [],'class' : ""}
html_class_map["Em"] =  {'dom' : "em",'attributes' : [],'class' : ""}
html_class_map["Link"] =  {'dom' : "a",'attributes' : [
	{'name' : "href",'value' : ""}
	],'class' : "" }
html_class_map["Quote"] =  {'dom' : "q",'attributes' : [],'class' : ""}
html_class_map["UL"] =  {'dom' : "ul",'attributes' : [],'class' : ""}
html_class_map["OL"] =  {'dom' : "ol",'attributes' : [],'class' : ""}
html_class_map["LI"] =  {'dom' : "li",'attributes' : [],'class' : ""}

html_class_map["KBD"] =  {'dom' : "kbd",'attributes' : [],'class' : ""}
//from here bootstrap labels starts
html_class_map["lbl_def"] =  {'dom' : "span",'attributes' : [],'class' : "label label-default"}
html_class_map["lbl_prim"] =  {'dom' : "span",'attributes' : [],'class' : "label label-primary"}
html_class_map["lbl_succ"] =  {'dom' : "span",'attributes' : [],'class' : "label label-success"}
html_class_map["lbl_info"] =  {'dom' : "span",'attributes' : [],'class' : "label label-info"}
html_class_map["lbl_warn"] =  {'dom' : "span",'attributes' : [],'class' : "label label-warning"}
html_class_map["lbl_dan"] =  {'dom' : "span",'attributes' : [],'class' : "label label-danger"}

//from here boostrap badges starts
html_class_map["badge"] =  {'dom' : "span",'attributes' : [],'class' : "badge"}

//Headers
html_class_map["H1"] =  {'dom' : "h1",'attributes' : [],'class' : ""}
html_class_map["H2"] =  {'dom' : "H2",'attributes' : [],'class' : ""}
html_class_map["H3"] =  {'dom' : "H3",'attributes' : [],'class' : ""}
html_class_map["H4"] =  {'dom' : "H4",'attributes' : [],'class' : ""}
html_class_map["H5"] =  {'dom' : "H5",'attributes' : [],'class' : ""}
html_class_map["H6"] =  {'dom' : "H6",'attributes' : [],'class' : ""}

//'dom' elements
html_class_map["Span"] =  {'dom' : "span",'attributes' : [],'class' : ""}
html_class_map["Div"] =  {'dom' : "Div",'attributes' : [],'class' : ""}
html_class_map["Paragraph"] =  {'dom' : "P",'attributes' : [],'class' : ""}
html_class_map["Block-Quote"] =  {'dom' : "blockquote",'attributes' : [],'class' : ""}
html_class_map["Pre"] =  {'dom' : "pre",'attributes' : [],'class' : ""}
html_class_map["Pre.modified"] =  {'dom' : "pre",'attributes' : [],'class' : "modified"}

//Java code
html_class_map["Java-Code"] =  {'dom' : "pre",'attributes' : [],'class' : "java"}
html_class_map["Java-Keywords"] =  {'dom' : "span",'attributes' : [],'class' : "keywords"}
html_class_map["Java-Comments"] =  {'dom' : "span",'attributes' : [],'class' : "comments"}
html_class_map["Java-Methods"] =  {'dom' : "span",'attributes' : [],'class' : "methods"}
html_class_map["Java-Class"] =  {'dom' : "span",'attributes' : [],'class' : "classNames"}
html_class_map["Java-Literals"] =  {'dom' : "span",'attributes' : [],'class' : "literals"}
html_class_map["Java-Identifiers"] =  {'dom' : "span",'attributes' : [],'class' : "identifiers"}
html_class_map["Java-Objects"] =  {'dom' : "span",'attributes' : [],'class' : "objects"}
html_class_map["Java-Annotations"] =  {'dom' : "span",'attributes' : [],'class' : "annotations"}

//Sass codes
html_class_map["SASS-Code"] =  {'dom' : "pre",'attributes' : [],'class' : "sass"}
html_class_map["SASS-Class"] =  {'dom' : "span",'attributes' : [],'class' : "class"}
html_class_map["SASS-Directives"] =  {'dom' : "span",'attributes' : [],'class' : "directives"}
html_class_map["SASS-Html-Tag"] =  {'dom' : "span",'attributes' : [],'class' : "htmltag"}
html_class_map["SASS-Literals"] =  {'dom' : "span",'attributes' : [],'class' : "literals"}
html_class_map["SASS-IdSelector"] =  {'dom' : "span",'attributes' : [],'class' : "idSelector"}
html_class_map["SASS-Styles"] =  {'dom' : "span",'attributes' : [],'class' : "styles"}
html_class_map["SASS-Color-Literals"] =  {'dom' : "span",'attributes' : [],'class' : "color-literals"}
html_class_map["SASS-Color-Code"] =  {'dom' : "span",'attributes' : [],'class' : "color-code"}
html_class_map["SASS-Comments"] =  {'dom' : "span",'attributes' : [],'class' : "comments"}
html_class_map["SASS-Variables"] =  {'dom' : "span",'attributes' : [],'class' : "variables"}
html_class_map["SASS-List"] =  {'dom' : "span",'attributes' : [],'class' : "list"}
html_class_map["SASS-Map"] =  {'dom' : "span",'attributes' : [],'class' : "map"}


$(document).ready(function(){


$(".text-editing,.headers,.dom-elements,.java-code,.sass-code").on("click",function(event){
	event.preventDefault()
	event.stopPropagation()
	//alert($(event.target).data("action"))
	replaceText($(event.target).data("action"))
})

$("#previewBtn").on("click",function(event){
	$('#textarea_preview').html($('#editor_textarea').val());
})

});



function replaceText(data_action){
	console.log("For :- "+data_action+" Found  Tag:- "+getOpeningTag(html_class_map[data_action])
		+"hello"+getClosingTag(html_class_map[data_action]))
	var openTag = getOpeningTag(html_class_map[data_action])
	var closeTag = getClosingTag(html_class_map[data_action])
	var selectedText = $('#editor_textarea').selection('get')
	$('#editor_textarea').selection('replace', {text : openTag + selectedText + closeTag });
	$('#textarea_preview').html($('#editor_textarea').val());
}

//Returns Opening tag eg. <pre class='java'>
function getOpeningTag(tagObject){
	var htmlOpenTag = "";

	//appending starting HTML '<'
	htmlOpenTag += "<" + tagObject['dom'] ;

	//appending class name if class name is not empty
	htmlOpenTag += tagObject['class'] != null ? (tagObject['class'].trim().length > 0 
	? " class='"+tagObject['class'].trim()+"'" : " ") : " " ;


	//Appending Attributes
	if(tagObject['attributes']){
		if(tagObject['attributes'].length > 0){
			$.each(tagObject['attributes'],function(index){
				var attribute = tagObject['attributes'][index]
				htmlOpenTag += " " + attribute['name'] +"='"+attribute['value']+"'"
			})
		}
	}

	//Appending closing '>' to properly end the html tag
	htmlOpenTag += " >"

	return htmlOpenTag
}

//Returns Closing Tag eg. </pre>
function getClosingTag(tagObject){
	var htmlCloseTag = "</"+tagObject['dom'] +">"
	return htmlCloseTag
}

